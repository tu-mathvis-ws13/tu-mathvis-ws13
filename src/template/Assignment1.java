package template;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.SwingConstants;

import charlesgunn.anim.gui.AnimationPanel;
import charlesgunn.anim.jreality.SceneGraphAnimator;
import charlesgunn.anim.util.AnimationUtility;
import charlesgunn.util.TextSlider;
import de.jreality.geometry.Primitives;
import de.jreality.math.MatrixBuilder;
import de.jreality.scene.Geometry;
import de.jreality.scene.SceneGraphComponent;
import de.jreality.scene.Sphere;
import de.jreality.scene.Viewer;
import de.jreality.shader.DefaultGeometryShader;
import de.jreality.shader.DefaultPolygonShader;
import de.jreality.shader.ShaderUtility;
import de.jreality.util.CameraUtility;
import de.jreality.util.Input;
import de.jreality.util.SceneGraphUtility;

/**
 * This first assignment is intended as a practice for 
 * 		1. learning about the class {@link MatrixBuilder},
 * 		2. using the animation manager, and 
 * 	    3. adding parameters to the inspector associated to the application.
 * 
 * The program constructs a square array of scene graph components each one containing a standard geometry,
 * by default, a colored cube.  These cubes fits together to form a large flat box.  
 * 
 * The assignment is equipped with an animation file which is read at start-up.
 * The user can invoke the animation manager by dragging the top window tab labeled "Animation" onto your desktop.
 * An animation panel should then be displayed. Click on the button labeled "Play" to play the
 * 
 * The animation is controlled by the method {@link Assignment1#setValueAtTime(double)}.  It changes the individual
 * transformations of the scene graph components making up the array, based on the animation time and a geometry parameter,
 * both of which take values between 0 and 1.  The parameter {@link Assignment1#waveDelay} controls how these two parameters are
 * combined into a single parameter.
 * 
 * Your assignment is to make a copy of this class in your package.
 * Then, extend the class in the following directions:
 *     1.  Change the function getGeometryParameter() so that the "level sets" are not diagonal lines but rather concentric circles
 *     2.  Change the method setValueAtTime() to assign other transformations.  
 *         To do this, you can define other parameters and use other methods on MatrixBuilder,
 *         for example, methods that implement translations, or non-isotropic scaling
 *     3.  Introduce a new parameter which has to do with your animation and insert it into the inspector (in {@link Assignment1#getInspector()},
 *     	   following the patterns of the parameters which are already present there.
 * @author Charles Gunn
 *
 */
public class Assignment1 extends Assignment {
	
	private int sizeOfArray = 50;		// make a square array this size
	int whichGeometry = 1;				// default is to display cube instead of sphere.
	double minRadius = .2, maxRadius = 1.0;		// parameters for controlling the default animation
	double waveDelay = .2;				// how the wave propagates through the array of geometry
	double hole = 4.0; 					// size of the hole of the torus
	
	private transient SceneGraphComponent worldSGC, 
		childArray[][] = new SceneGraphComponent[sizeOfArray][sizeOfArray],
		leafChild = new SceneGraphComponent("leaf child");
	private transient Geometry[] geoms = {new Sphere(), Primitives.coloredCube()};
	
	@Override
	public SceneGraphComponent getContent() {
		worldSGC = SceneGraphUtility.createFullSceneGraphComponent("Assignment 1");
		// disable display of vertices and edges
		DefaultGeometryShader dgs = ShaderUtility.createDefaultGeometryShader(worldSGC.getAppearance(), true);
		dgs.setShowLines(false);
		dgs.setShowPoints(false);
		DefaultPolygonShader dps = (DefaultPolygonShader) dgs.createPolygonShader("default");
		dps.setDiffuseColor(Color.white);

		// Set up a single scene graph component which contains the geometry for the whole scene graph.
		leafChild.setGeometry(geoms[whichGeometry]);

		// set up the array of scene graph components
		setupArray();
		
		// have to make sure we don't let the scene graph animator set key frames on
		// our scene graph, since we animate it by hand in the method setValueAtTime() below.
		worldSGC.getAppearance().setAttribute(SceneGraphAnimator.ANIMATED, false);
		return worldSGC;
	}
	
	private double cos(double x){
	return 2*Math.cos(Math.PI*2.0*x/(sizeOfArray-1.0));	
	}
	
	private double sin(double x){
	return 2*Math.sin(Math.PI*2.0*x/(sizeOfArray-1.0));	
	}

	// set up an array of scene graph components, each having as child the leafChild instance
	// This allows us to change the geometry by changing this single instance rather than 
	// having to go through the whole array.
	private void setupArray() {
//		worldSGC.removeAllChildren();
		DefaultGeometryShader dgs;
		DefaultPolygonShader dps;
		childArray = new SceneGraphComponent[sizeOfArray][sizeOfArray];
		float dstep = 1f/sizeOfArray;			// this is used to construct 
		for (int i = 0; i<sizeOfArray; ++i)	{
			for (int j = 0; j<sizeOfArray; ++j)	{
				SceneGraphComponent translated = SceneGraphUtility.createFullSceneGraphComponent("tlated"+i+j);
				SceneGraphComponent child = SceneGraphUtility.createFullSceneGraphComponent("child"+i+j);
				childArray[i][j] = child;
				child.addChild(leafChild);
				translated.addChild(child);
				// translate this component into an array with step size 2 
				//PLANE
				//MatrixBuilder.euclidean().translate(2*i, 2*j, 0).assignTo(translated);
				//CYLINDER
				//MatrixBuilder.euclidean().translate(sizeOfArray+2*cos(i),sizeOfArray+2*sin(i),sizeOfArray-2*j).rotateZ(Math.PI*2.0*i/(sizeOfArray-1.0)).assignTo(translated);
				//TORUS
				MatrixBuilder.euclidean().translate(10.0+(hole+cos(j))*cos(i),10.0+(hole+cos(j))*sin(i),sin(j)).rotateZ(Math.PI*2.0*i/(sizeOfArray-1.0)).assignTo(translated);
				worldSGC.addChild(translated);
				// construct a color based on the (x,y) position in the array
				// the x-value varies the red channel; the y-value varies the green channel
				// currently the appearance only has an effect when spheres are displayed!
				Color color = new Color((float) i*dstep, (float) j * dstep, .75f);
				dgs = ShaderUtility.createDefaultGeometryShader(child.getAppearance(), true);
				dps = (DefaultPolygonShader) dgs.createPolygonShader("default");
				dps.setDiffuseColor(color);
			}
		}
	}
	
	/**
	 * The following method is the basic method for the animation system.  
	 * Here we create a wave of motion moving through the array of geometries
	 */
	@Override
	public void setValueAtTime(double time) {
		// TODO Auto-generated method stub
		super.setValueAtTime(time);
		for (int i = 0; i<sizeOfArray; ++i)	{
			for (int j = 0; j<sizeOfArray; ++j)	{
				// diagonal varies between 0 and 1
				double geometryParameter = getGeometryParameter(i, j);
				// waveParameter varies between 0 and 1; input values less than 0 and greater than 1-waveDelay are clipped
				double waveParameter = AnimationUtility.linearInterpolation(time-waveDelay*geometryParameter, 0, 1-waveDelay, 0, 1);
				// calculate the radius from the waveParameter
				double radius = scalingFactor(waveParameter);
				// change the transformation associated to this scene graph component based on the
				// derived parameters.  
				//	   radius determines the scaling, 
				//	   waveParameter determines a rotation around the y-axis.
				double angle = waveParameter*2.0*Math.PI;
				//MatrixBuilder.euclidean().rotateY(angle).scale(radius, radius, radius).assignTo(childArray[i][j]);
				MatrixBuilder.euclidean().translate(Math.sin(angle),Math.sin(2*angle),Math.sin(3*angle)).rotate(angle,Math.sin(angle),Math.sin(2*angle),Math.sin(3*angle)).scale(radius,radius,radius).assignTo(childArray[i][j]);
			}
		}
	}

	/**
	 * This function returns a value between 0 and 1 based on the (i,j) coordinates in the array
	 */

	private double sq(double i){
		return i*i;
	}
	
	private double getGeometryParameter(int i, int j) {
		return (sq(sizeOfArray/2-i)+sq(sizeOfArray/2-j))/(2*sq(sizeOfArray/2));
		//return (j)/(1.0*(sizeOfArray-1.0));
		//return (i)/(1.0*(sizeOfArray-1.0));
	}

	// a linear function such that f(0) = f(1) = 1.0, and f(.5) = 1.0;
	private double scalingFactor(double x)	{
		// return 1.0;
		// un-comment the following out to get an "upside-down" tent function
	double inverseTent = 2*Math.abs(x-.5);
	return AnimationUtility.linearInterpolation(inverseTent, 0.0, 1.0, minRadius, maxRadius);
	}
	
	/**
	 * This is the primary method which causes everything else to happen.  It's called 
	 * from the main() method below.
	 */
	@Override
	public void display() {
		// TODO Auto-generated method stub
		super.display();
		Viewer viewer = jrviewer.getViewer();
		CameraUtility.encompass(viewer);
		addKeyListener(viewer);
		// read in the animation file
		try {
			animationPlugin.getAnimationPanel().read(new Input(this.getClass().getResource("assg1-anim.xml")));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		animationPlugin.getAnimationPanel();
		AnimationPanel.setResourceDir("src/template");
	}
	
	/**
	 * This is the file where the application properties are stored.
	 * To make sure this is updated each time you run the application,
	 * use the "Quit" menu item on the "File" menu to exit.
	 */
	@Override
	public File getPropertyFile() {
		return  null; //new File("src/template/assignment1.xml");
	}

	/**
	 * Provided as an example of how to provide a key listener to an application
	 * @param viewer
	 */
	private void addKeyListener(Viewer viewer) {
		Component comp = ((Component) viewer.getViewingComponent());
		comp.addKeyListener(new KeyAdapter() {
 				public void keyPressed(KeyEvent e)	{ 
					switch(e.getKeyCode())	{
						
					case KeyEvent.VK_H:
						System.err.println("Key listener: \n\t1: toggle display of sphere");
						break;
		
					case KeyEvent.VK_1:
						whichGeometry = (whichGeometry + 1)%geoms.length;
						leafChild.setGeometry(geoms[whichGeometry]);						
						break;
		
				}
 				}
			});
	}
	
	/**
	 * Currently don't have a documentation file
	 */
	@Override
	public String getDocumentationFile() {
		// TODO Auto-generated method stub
		return  "Assignment1.html";
	}


	@Override
	public Component getInspector() {
		// TODO Auto-generated method stub
		Box vbox = Box.createVerticalBox();
		JCheckBox whichGeomB = new JCheckBox("sphere");
		whichGeomB.setSelected(whichGeometry == 0);
		whichGeomB.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				boolean boo = ((JCheckBox) arg0.getSource()).isSelected();
				whichGeometry = boo ? 0 : 1;
				leafChild.setGeometry(geoms[whichGeometry]);						
			}
		});
		vbox.add(whichGeomB);
		
		TextSlider sizeSlider = new TextSlider.Integer("size", SwingConstants.HORIZONTAL, 2, 60, sizeOfArray);
		sizeSlider.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				sizeOfArray = ((TextSlider)e.getSource()).getValue().intValue();
				contentPlugin.setContent(getContent());
			}
		});
		vbox.add(sizeSlider);
		TextSlider minRSlider = new TextSlider.Double("min radius", SwingConstants.HORIZONTAL, 0.05, 1.0, minRadius);
		minRSlider.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				minRadius = ((TextSlider)e.getSource()).getValue().doubleValue();
			}
		});
		vbox.add(minRSlider);
		TextSlider maxRSlider = new TextSlider.Double("max radius", SwingConstants.HORIZONTAL,0.05, 1.0, maxRadius);
		maxRSlider.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				maxRadius = ((TextSlider)e.getSource()).getValue().doubleValue();
			}
		});
		vbox.add(maxRSlider);
		TextSlider wavePSlider = new TextSlider.Double("wave delay", SwingConstants.HORIZONTAL,0.0,.99, waveDelay);
		wavePSlider.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				waveDelay = ((TextSlider)e.getSource()).getValue().doubleValue();
			}
		});
		vbox.add(wavePSlider);
		return vbox;
	}

	public static void main(String[] args)		{
		Assignment1 theProgram = new Assignment1();
		theProgram.display();
	}
	


}
