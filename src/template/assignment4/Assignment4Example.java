package template.assignment4;


import de.jreality.math.Matrix;
import de.jreality.math.MatrixBuilder;
import de.jreality.math.Pn;
import de.jreality.scene.SceneGraphComponent;
import de.jtem.discretegroup.core.DiscreteGroupConstraint;
import de.jtem.discretegroup.core.DiscreteGroupElement;
import de.jtem.discretegroup.core.DiscreteGroupSimpleConstraint;
import de.jtem.discretegroup.groups.FriezeGroup;



import charlesgunn.jreality.geometry.projective.PointCollector;
import de.jreality.math.Matrix;
import de.jreality.math.MatrixBuilder;
import de.jreality.math.Pn;
import de.jreality.scene.SceneGraphComponent;
import de.jreality.util.Color;
import de.jtem.discretegroup.core.DiscreteGroupConstraint;
import de.jtem.discretegroup.core.DiscreteGroupElement;
import de.jtem.discretegroup.core.DiscreteGroupSimpleConstraint;
import de.jtem.discretegroup.groups.FriezeGroup;
import discreteGroup.demo.FundamentalDomainsDemo;

public class Assignment4Example extends AbstractAssignment4 {

	@Override
	public AbstractAssignment4PaintTool getPaintTool() {
		return new Assignment4PaintTool(canvasSGC, this);
	}


	@Override
	// this method sets the matrices in the scene graph based on the parameters and the group
	protected void updateCopies() {
		Matrix[] cosets = null;
		Matrix generatingTranslation = null;
		int numCosets = 0;
		// I've implemented the "hard" approach for the simplest frieze group,
		// and the "easy" approach, using the discretegroup package, for the rest.  
		// Your assignment is to implement the groups 1 and 6 the "hard way"
		switch(whichGroup)	{
		case 0:
			numCosets = 1;
			cosets = new Matrix[1];
			cosets[0] = new Matrix();
			generatingTranslation = MatrixBuilder.euclidean().translate(1, 0, 0).getMatrix();
//			// calculate negative and positive powers of the translation and
//			// assign to the children
//			for (int j = 0; j < count; ++j) {
//				SceneGraphComponent child = translationListSGC.getChildComponent(j);
//				Matrix.power(tlate, j - count/2).assignTo(child);
//			}
			break;
		case 1:
			numCosets = 2;
			cosets = new Matrix[numCosets];
			cosets[0] = new Matrix(); // identity
			cosets[1] = MatrixBuilder.euclidean().reflect(new double[]{1,0,0,0}).getMatrix();
			generatingTranslation = MatrixBuilder.euclidean().translate(2, 0, 0).getMatrix();
			break;
			// implement this group (* infinity infinity ) here as above in case 0
			// there are two vertical mirrors a distance 1 apart.
		case 3:
			generatingTranslation = MatrixBuilder.euclidean().reflect(new double[]{0,1,0,0}).translate(1,0,0).getMatrix();
			numCosets = 1;
			cosets = new Matrix[1];
			cosets[0] = new Matrix();
			break;
		case 6:
			// implement this group (* 2 2 infinity) here as above in case 0
			// there are two vertical mirrors a distance 1 apart and a horizontal mirror  in the line y = 0.
			generatingTranslation = MatrixBuilder.euclidean().translate(2,0,0).getMatrix();
			numCosets = 4;
			cosets = new Matrix[4];
			cosets[0] = new Matrix();
			cosets[1] = MatrixBuilder.euclidean().reflect(new double[]{1,0,0,0}).getMatrix();
			cosets[2] = MatrixBuilder.euclidean().reflect(new double[]{0,1,0,0}).getMatrix();
			cosets[3] = MatrixBuilder.euclidean().rotateZ(Math.PI).getMatrix();
			break;
		case 2:
		case 4:
		case 5:
		default:
			FriezeGroup fg = FriezeGroup.instanceOfGroup(FriezeGroup.friezeNames[whichGroup],1.0, Pn.EUCLIDEAN);
			DiscreteGroupConstraint dgc = new DiscreteGroupSimpleConstraint(count);
			fg.setConstraint(dgc);
			fg.update();
			
			DiscreteGroupElement[] list = fg.getElementList();
			for (int j = 0; j < count; ++j) {
				SceneGraphComponent child = translationListSGC.getChildComponent(j);
				Matrix tm = new Matrix(list[j].getArray());
				tm.assignTo(child);
			}
			// if there is a horizontal mirror, separate the two rows by translating the fundamental domain up
			break;
		}
		if (numCosets > 0) {
			int outerLoopCount = count/numCosets, counter = 0;
			// run through the powers of the generating translation ...
			for (int j = 0; j <= outerLoopCount; ++j) {
				Matrix powerOfTranslation = Matrix.power(generatingTranslation, (j - outerLoopCount/2));
				// and multiply it on the right by each of the coset representatives
				for (int k = 0; k<numCosets; ++k)	{
					// skip over non-existing scene graph components at end
					if (counter >= count) continue;
					SceneGraphComponent child = translationListSGC.getChildComponent(counter++);
					Matrix tmp = new Matrix(cosets[k].getArray().clone());
					tmp.multiplyOnLeft(powerOfTranslation);
					tmp.assignTo(child);
				}
			}

		}
		// For the last three groups, raise the fundamental domain up so a second row of copies fits below the x-axis.
		MatrixBuilder.euclidean().translate(0, (whichGroup >=  4 && whichGroup <= 6) ? .5 : 0, 0).assignTo(fundamentalDomain);
	}


	// create a spiral of points around the origin;  the spiral goes in and out 
	// between two radial limits
	double r = .2, R = 1, radius = R;
	boolean in = true;
	double[] point = {0,0,0,1};
	double factor = .99;
	int curveCount = 0;
	@Override
	public void setValueAtTime(double d) {
		super.setValueAtTime(d);
//		pointCollector.addPoint(point);
//		radius = in ? factor * radius : radius/factor;
//		double angle = (curveCount)/(10.0);
//		curveCount++;
//		point = new double[]{ radius * Math.cos(angle), radius * Math.sin(angle), 0, 1.0}; 
//		if (radius < r)  in = false;
//		if (radius > R) in = true;
	}

	@Override
	public void display() {
		// TODO Auto-generated method stub
		super.display();
		jrviewer.getViewer().getSceneRoot().getAppearance().setAttribute("backgroundColor", new Color(125, 125, 125));
	}

	public static void main(String[] args) {
		new Assignment4Example().display();
	}

/// branch 'master' of git@gitorious.org:tu-mathvis-ws13/tu-mathvis-ws13.git

}
