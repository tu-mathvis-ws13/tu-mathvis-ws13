package template.assignment4;



import static de.jreality.shader.CommonAttributes.DIFFUSE_COLOR;
import static de.jreality.shader.CommonAttributes.POLYGON_SHADER;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;

import charlesgunn.anim.jreality.SceneGraphAnimator;

import template.Assignment;
import de.jreality.geometry.Primitives;
import de.jreality.jogl.shader.ShadedSphereImage;
import de.jreality.math.MatrixBuilder;
import de.jreality.math.Rn;
import de.jreality.scene.Geometry;
import de.jreality.scene.IndexedFaceSet;
import de.jreality.scene.SceneGraphComponent;
import de.jreality.scene.pick.PickResult;
import de.jreality.scene.tool.AbstractTool;
import de.jreality.scene.tool.InputSlot;
import de.jreality.scene.tool.Tool;
import de.jreality.scene.tool.ToolContext;
import de.jreality.shader.CommonAttributes;
import de.jreality.shader.DefaultGeometryShader;
import de.jreality.shader.DefaultPolygonShader;
import de.jreality.shader.ImageData;
import de.jreality.shader.RenderingHintsShader;
import de.jreality.shader.ShaderUtility;
import de.jreality.shader.Texture2D;
import de.jreality.shader.TextureUtility;
import de.jreality.util.CameraUtility;
import de.jreality.util.SceneGraphUtility;
import de.jtem.discretegroup.core.DiscreteGroupUtility;
import de.jtem.discretegroup.groups.FriezeGroup;
import de.jtem.discretegroup.util.TextSlider;
import de.jtem.discretegroup.util.TranslateTool;

abstract public class AbstractAssignment4 extends Assignment {

	transient protected SceneGraphComponent worldSGC, 
		translationListSGC,
		fundamentalDomain,
		elSGC,
		canvasSGC = SceneGraphUtility.createFullSceneGraphComponent("canvas");
	
	protected int count = 20,
			whichGroup = 0;
	
	boolean doPaint = false;
	
	// these are the names of the 7 frieze groups.   
	static Character infinity = '\u221e';

	protected AbstractAssignment4PaintTool 	paintTool = getPaintTool();
;

	@Override
	public SceneGraphComponent getContent() {
		if (worldSGC != null) return worldSGC;
		
		worldSGC = SceneGraphUtility.createFullSceneGraphComponent("Assignment 2");
		worldSGC.getAppearance().setAttribute(SceneGraphAnimator.ANIMATED, false);
		DefaultGeometryShader dgs = ShaderUtility.createDefaultGeometryShader(worldSGC.getAppearance(), true);
		dgs.setShowLines(false);
		dgs.setShowPoints(true);
		DefaultPolygonShader dps = (DefaultPolygonShader) dgs.createPolygonShader("default");
		RenderingHintsShader rhs = ShaderUtility.createDefaultRenderingHintsShader(worldSGC.getAppearance(), true);
		rhs.setLightingEnabled(false);
		translationListSGC = SceneGraphUtility.createFullSceneGraphComponent("list");
		worldSGC.addChild(translationListSGC);
		// create a simple geometry in the shape of the letter "L"; it has not internal symmetries so looks 
		// different when rotated or reflected.  It is initially positioned so it is centered at 
		// circa (.5,0), has width .5 and height .5*golden ratio. 
		elSGC = SceneGraphUtility.createFullSceneGraphComponent("L");
		SceneGraphComponent elkit = DiscreteGroupUtility.getElKit();
        MatrixBuilder.euclidean().translate(.3, -.4,0).scale(.5).assignTo(elkit);
		dgs = ShaderUtility.createDefaultGeometryShader(elkit.getAppearance(), true);
		dgs.setShowFaces(true);
		dgs.setShowLines(false);
		dgs.setShowPoints(false);
		dps = (DefaultPolygonShader) dgs.createPolygonShader("default");
        dps.setDiffuseColor(new Color(200, 255, 0));
		elSGC.addChild(elkit);
		elSGC.addTool(new TranslateTool());
		elSGC.setVisible(!doPaint);
		// the fundamental domain contains all the geometry which will be replicated in the pattern
		// one could put other items in here, not just the letter "L"
		fundamentalDomain = SceneGraphUtility.createFullSceneGraphComponent("fundamental domain");
		fundamentalDomain.addChild(elSGC);

		setupPainting();
		
		// set up the scene graph for the translated copies
		initCopies();
		updateCopies();
		return worldSGC;
	}

	public int getWhichGroup() {
		return whichGroup;
	}

	public abstract AbstractAssignment4PaintTool getPaintTool();
	
	// this method sets up the scene graph but doesn't set the matrices
	protected void initCopies() {
		translationListSGC.removeAllChildren();
		// set up the copies, based on the count parameter
		// the matrices for this copies are set in updateCopies() below
		for (int j = 0; j < count; ++j)	{
			SceneGraphComponent child = SceneGraphUtility.createFullSceneGraphComponent("child"+j);
			translationListSGC.addChild(child);
			child.addChild(fundamentalDomain);
		}
	}

	private void setupPainting()	{
		IndexedFaceSet canvas = Primitives.texturedQuadrilateral();
		canvasSGC.setGeometry(canvas);
		fundamentalDomain.addChild(canvasSGC);
		canvasSGC.getAppearance().setAttribute(POLYGON_SHADER+"."+DIFFUSE_COLOR, new Color(255, 255,255));
		canvasSGC.getAppearance().setAttribute(CommonAttributes.VERTEX_DRAW, false);
		canvasSGC.getAppearance().setAttribute(CommonAttributes.EDGE_DRAW, false);
		MatrixBuilder.euclidean().translate(0,-.5,0).assignTo(canvasSGC);
		canvasSGC.addTool(paintTool);		
		canvasSGC.setVisible(doPaint);
	}

	abstract protected void updateCopies();
//	abstract protected Geometry getAnimatedGeometry();
	
	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
		initCopies();
		updateCopies();
	}

	@Override
	public File getPropertyFile() {
		return new File("src/template/assignment4/assignment4.xml");
	}

	@Override
	public String getDocumentationFile() {
		return "Assignment4.html";
	}
	
	@Override
	public void display() {
		super.display();
		CameraUtility.encompass(jrviewer.getViewer());
	}

	@Override
	public Component getInspector() {
		Box vbox = Box.createVerticalBox();
		
		// Allow the use to choose a frieze group, and call updateCopies()
		// to deal with this choice.
		JComboBox groupscb = new JComboBox(FriezeGroup.friezeNames);
		groupscb.setSelectedIndex(whichGroup);
		groupscb.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				whichGroup = ((JComboBox) arg0.getSource()).getSelectedIndex();
				updateCopies();
			}
		});
		Box hbox = Box.createHorizontalBox();
		hbox.add(Box.createHorizontalGlue());
		hbox.add(groupscb);
		hbox.add(Box.createHorizontalGlue());
		final JRadioButton paintB = new JRadioButton("paint");
		paintB.setSelected(doPaint);
		paintB.addActionListener(new ActionListener()	{
			public void actionPerformed(ActionEvent e)	{
				doPaint = ((JRadioButton) e.getSource()).isSelected();
				elSGC.setVisible(!doPaint);
				canvasSGC.setVisible(doPaint);
				jrviewer.getViewer().renderAsync();
			}
		});
		hbox.add(paintB);
		hbox.add(Box.createHorizontalGlue());

		vbox.add(hbox);

		final TextSlider.Integer sizeSlider = new TextSlider.Integer("count",SwingConstants.HORIZONTAL, 1, 40, count);
		sizeSlider.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = ((TextSlider) arg0.getSource()).getValue().intValue();
				setCount(i);
			}
		});
		vbox.add(sizeSlider);
				
		vbox.add(paintTool.getInspector());
		/// branch 'master' of git@gitorious.org:tu-mathvis-ws13/tu-mathvis-ws13.git
		return vbox;
	}


}
