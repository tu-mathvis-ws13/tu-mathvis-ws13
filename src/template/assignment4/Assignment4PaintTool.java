package template.assignment4;

import de.jreality.scene.SceneGraphComponent;

public class Assignment4PaintTool extends AbstractAssignment4PaintTool {

	public Assignment4PaintTool(SceneGraphComponent sgc, AbstractAssignment4 a) {
		super(sgc, a);
	}

	@Override
	public void paintBrush(int cx, int cy, int group) {
		super.paintBrush(cx, cy, group);
		switch(group)	{
		case 0:
			// when the brush gets near the right boundary, draw it translated to the left
			if (imageSize - cx < brushSize/2)
				g2d.drawImage(brush, -imageSize + cx - brushSize/2, cy - brushSize/2, null);
			// similarly for the left boundary
			if ( cx < brushSize/2)
				g2d.drawImage(brush, imageSize + cx - brushSize/2, cy - brushSize/2, null);
			break;
		default:
			break;
		}
	}

	
}
