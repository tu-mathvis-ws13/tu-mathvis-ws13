package template.assignment4;

import static de.jreality.shader.CommonAttributes.POLYGON_SHADER;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import charlesgunn.jreality.texture.SimpleTextureFactory;
import charlesgunn.jreality.texture.SimpleTextureFactory.TextureType;

import de.jreality.jogl.shader.ShadedSphereImage;
import de.jreality.math.Rn;
import de.jreality.plugin.JRViewer;
import de.jreality.scene.SceneGraphComponent;
import de.jreality.scene.pick.PickResult;
import de.jreality.scene.tool.AbstractTool;
import de.jreality.scene.tool.InputSlot;
import de.jreality.scene.tool.ToolContext;
import de.jreality.shader.ImageData;
import de.jreality.shader.Texture2D;
import de.jreality.shader.TextureUtility;
import de.jtem.discretegroup.util.TextSlider;

/**
 * The continuation of Assignment4 involves extending this class to take into account the boundary "gluings" of the
 * seven frieze groups.  Look at the switch statement in the {@link #perform(ToolContext)} method below.  I've included
 * code for the first frieze group -- you just need to uncomment it out to activate it. Verify that the code in fact
 * solves the problem for this group.  Your task is then to 
 * implement the corresponding code for the remaining 6 groups so that when the brush crosses a boundary of 
 * the fundamental region, the user doesn't notice anything.  To make it easier to see the seams, I've outlined the
 * fundamental region in black, but you can turn that off from the inspector.
 * 
 * Notice that the fundamental domain is the same for all 7 groups: the unit square with corners (0,0) to (1,1). For the first
 * 4 groups this has been translated down so that it is centered on the x-axis, hence for these cases the order-2 rotation
 * centers as well as the glide-reflection axis lies on the y = .5 line; for the remaining 3 groups it is left alone so
 * that a second row of images appear below the x-axis, hence for these groups the order-2 rotation centers as well
 * as the glide-reflection axis lie on the y = 0 line.  These facts determine the gluing identities along the border. 
 * @author gunn
 *
 */
public class AbstractAssignment4PaintTool extends AbstractTool {
	
	protected int imageSize = 512,
	  	brushSize = 64;
	protected double transparency = .5;
	protected boolean showBoundary = true, shadedSphere = false;
	protected Graphics2D g2d;
	protected BufferedImage brush, paintBrush, eraser;
	Color canvasColor = new Color(1f, 1f, 1f, 1f);;
	Color brushColor = new Color(100, 100, 255,((int)(255*transparency)));
	AbstractAssignment4 assignment;
	private BufferedImage canvasBI;

	public AbstractAssignment4PaintTool(SceneGraphComponent sgc,  AbstractAssignment4 a)	{
		super(InputSlot.LEFT_BUTTON, InputSlot.SHIFT_LEFT_BUTTON);
		assignment = a;
		canvasBI = new BufferedImage(imageSize, imageSize, BufferedImage.TYPE_INT_ARGB);
		ImageData id = new ImageData(canvasBI);
//		// this is not pretty: the original bi was trashed to get a different byte order.
		canvasBI = (BufferedImage) id.getImage();
		initCanvas();
		
		// set up the texture object
		final Texture2D tex2d = TextureUtility.createTexture(sgc.getAppearance(), POLYGON_SHADER,id);
		tex2d.setRepeatS(Texture2D.GL_CLAMP_TO_EDGE);
		tex2d.setRepeatT(Texture2D.GL_CLAMP_TO_EDGE);
 		tex2d.setAnimated(true);
 		// not a good idea to set false: no picture shows up!
 		tex2d.setMipmapMode(true);
 		tex2d.setApplyMode(Texture2D.GL_MODULATE);
 		initBrush();

	}

	private void initCanvas() {
		g2d = canvasBI.createGraphics();
		g2d.setColor(canvasColor);
		g2d.fillRect(0, 0, imageSize, imageSize);
		if (showBoundary)	{
			g2d.setColor(Color.black);
			g2d.fillRect(0, 0, 1, imageSize);
			g2d.fillRect(imageSize-1, 0, imageSize, imageSize);
			g2d.fillRect(0, 0, imageSize, 1);
			g2d.fillRect(0, imageSize-1, imageSize, imageSize);
		}
	}

	private void initBrush() {
 		// make a transparent brush using utility method in jogl backend
		brushColor =  new Color(brushColor.getRed(), brushColor.getGreen(), brushColor.getBlue(),
				((int)(255*(1-transparency))));
 		ImageData bid = null;
 		if (shadedSphere) {
 			bid = ShadedSphereImage.shadedSphereImage(
 					Rn.setToLength(null, new double[]{0,1,1}, 1.0),
 	 				brushColor, 
 	 				Color.white, 
 	 				10.0, 
 	 				brushSize, 
 	 				true,
 	 				new int[]{1,0,3,2});
		} else {
	 		SimpleTextureFactory stf = new SimpleTextureFactory();
			stf.setSize(brushSize);
			stf.setType(TextureType.DISK);
			stf.setColor0(brushColor);
			stf.setChannels(new int[]{1,0,3,2});
			stf.update();
			bid = stf.getImageData();			
		}
 		paintBrush = (BufferedImage) bid.getImage();
 		System.err.println("initializing paint brush");
		eraser = new BufferedImage(brushSize, brushSize, BufferedImage.TYPE_INT_ARGB);
		Graphics2D tmpG = (Graphics2D) eraser.getGraphics();
		tmpG.setColor(canvasColor);
		tmpG.fillRect(0, 0, brushSize, brushSize);
	}
	
	public void activate(ToolContext tc) {
		// express interest in mouse moves (so perform() gets called)
   		addCurrentSlot(InputSlot.getDevice("PointerTransformation"));
   		if (tc.getSource() == InputSlot.LEFT_BUTTON) {
   			System.err.println("left mouse activate");
   			brush = paintBrush;
   		} else //brush = paintBrush;
   			initCanvas();

	}

	public void perform(ToolContext tc) {
		PickResult currentPick = tc.getCurrentPick();
		if (currentPick == null) return;
		double[] uv = currentPick.getObjectCoordinates();
		// that the following can happen is ... odd
		if (uv == null  || uv.length < 2) return;
		int ix = (int) (uv[0] * imageSize);
		int iy = (int) (uv[1] * imageSize);
		paintBrush(ix, iy, assignment.whichGroup);
		// and draw other copies depending on whether we are near the boundary of the fundamental domain
		// we have to explicitly trigger render since the scene graph isn't changed by the painting
		assignment.getJrviewer().getViewer().renderAsync();
	}
	
	public void paintBrush(int cx, int cy, int group) {
		// always draw the one standard brush
		g2d.drawImage(paintBrush, cx - brushSize/2, cy - brushSize/2, null);

	}
	
	public Component getInspector()	{
		Box vbox = Box.createVerticalBox();

		vbox.setBorder(new CompoundBorder(new EmptyBorder(5, 5, 5, 5),
				BorderFactory.createTitledBorder(BorderFactory
						.createEtchedBorder(), "Painting")));

		final TextSlider.Integer sizeSlider = new TextSlider.Integer("brush size",SwingConstants.HORIZONTAL, 1, 256, brushSize);
		sizeSlider.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				brushSize = ((TextSlider) arg0.getSource()).getValue().intValue();
				initBrush();
			}
		});
		vbox.add(sizeSlider);

		final TextSlider.Double transpSlider = new TextSlider.Double("transparency",SwingConstants.HORIZONTAL, 0, 1, transparency);
		transpSlider.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				transparency = ((TextSlider) arg0.getSource()).getValue().doubleValue();
				initBrush();
			}
		});
		vbox.add(transpSlider);

		final JButton colorsb = new JButton("brush color");
		colorsb.setOpaque(true);
		colorsb.setBackground(brushColor);
		colorsb.addActionListener(new ActionListener()	{
			public void actionPerformed(ActionEvent e)	{
				brushColor = JColorChooser.showDialog((Component) assignment.getJrviewer().getViewer().getViewingComponent(), "Select color ",  null);
				colorsb.setBackground(brushColor);
				initBrush();
			}
		});
		final JRadioButton showBndB = new JRadioButton("show boundary");
		showBndB.addActionListener(new ActionListener()	{
			public void actionPerformed(ActionEvent e)	{
				showBoundary = ((JRadioButton) e.getSource()).isSelected();
				initCanvas();
			}
		});

		final JRadioButton shadedSphereB = new JRadioButton("shaded Sphere");
		showBndB.addActionListener(new ActionListener()	{
			public void actionPerformed(ActionEvent e)	{
				shadedSphere = ((JRadioButton) e.getSource()).isSelected();
				initBrush();
			}
		});

		Box hbox = Box.createHorizontalBox();
		hbox.add(Box.createHorizontalGlue());
		hbox.add(colorsb);
		hbox.add(Box.createHorizontalGlue());
		hbox.add(showBndB);
		hbox.add(Box.createHorizontalGlue());
		hbox.add(shadedSphereB);
		hbox.add(Box.createHorizontalGlue());
		vbox.add(hbox);

		return vbox;
	}

	public String getDescription(InputSlot slot) {
		return null;
	}

	public String getDescription() {
		return "A tool which paints on a surface";
	}

	@Override
	public void deactivate(ToolContext tc) {
   		removeCurrentSlot(InputSlot.getDevice("PointerTransformation"));
	}

}
