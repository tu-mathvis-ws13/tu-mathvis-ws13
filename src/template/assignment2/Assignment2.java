package template.assignment2;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.SwingConstants;

import template.Assignment;

import charlesgunn.anim.gui.AnimationPanel;
import charlesgunn.anim.jreality.SceneGraphAnimator;
import charlesgunn.anim.util.AnimationUtility;

import charlesgunn.jreality.tools.TranslateShapeTool;

import charlesgunn.util.TextSlider;
import de.jreality.geometry.Primitives;
import de.jreality.math.Matrix;
import de.jreality.math.MatrixBuilder;
import de.jreality.scene.Geometry;
import de.jreality.scene.SceneGraphComponent;
import de.jreality.scene.Sphere;

import de.jreality.scene.Viewer;

import de.jreality.scene.Transformation;
import de.jreality.scene.Viewer;
import de.jreality.scene.tool.Tool;

import de.jreality.shader.DefaultGeometryShader;
import de.jreality.shader.DefaultPolygonShader;
import de.jreality.shader.ShaderUtility;
import de.jreality.util.CameraUtility;
import de.jreality.util.Input;
import de.jreality.util.SceneGraphUtility;
import de.jtem.jrworkspace.plugin.Controller;

/**
 * This first assignment is intended as a practice for 
 * 		1. learning about the class {@link MatrixBuilder},
 * 		2. using the animation manager, and 
 * 	    3. adding parameters to the inspector associated to the application.
 * 
 * The program constructs a square array of scene graph components each one containing a standard geometry,
 * by default, a colored cube.  These cubes fits together to form a large flat box.  
 * 
 * The assignment is equipped with an animation file which is read at start-up.
 * The user can invoke the animation manager by dragging the top window tab labeled "Animation" onto your desktop.
 * An animation panel should then be displayed. Click on the button labeled "Play" to play the
 * default animation. 
 * 
 * The animation is controlled by the method {@link Assignment2#setValueAtTime(double)}.  It changes the individual
 * transformations of the scene graph components making up the array, based on the animation time and a geometry parameter,
 * both of which take values between 0 and 1.  The parameter {@link Assignment2#waveDelay} controls how these two parameters are
 * combined into a single parameter.
 * 
 * Your assignment is to make a copy of this class in your package.
 * Then, extend the class in the following directions:
 *     1.  Change the function getGeometryParameter() so that the "level sets" are not diagonal lines but rather concentric circles
 *     2.  Change the method setValueAtTime() to assign other transformations.  
 *         To do this, you can define other parameters and use other methods on MatrixBuilder,
 *         for example, methods that implement translations, or non-isotropic scaling
 *     3.  Introduce a new parameter which has to do with your animation and insert it into the inspector (in {@link Assignment2#getInspector()},
 *     	   following the patterns of the parameters which are already present there.
 * @author Charles Gunn
 *
 */
abstract public class Assignment2 extends Assignment {
	
	protected int sizeOfArray = 11;		// make a square array this size
	protected int whichGeometry = 1;				// default is to display cube instead of sphere.
	protected double minRadius = .2, maxRadius = 1.0;		// parameters for controlling the default animation
	protected double waveDelay = .5;				// how the wave propagates through the array of geometry


	public transient SceneGraphComponent worldSGC1;
	protected transient SceneGraphComponent childArray1[][] = new SceneGraphComponent[sizeOfArray][sizeOfArray];
	protected transient SceneGraphComponent leafChild1 = new SceneGraphComponent("leaf child");
	public transient Geometry[] geoms1 = {new Sphere(), Primitives.coloredCube()};

	protected boolean activateLeafTool = false;

	private transient SceneGraphComponent worldSGC, 
		childArray[][] = new SceneGraphComponent[sizeOfArray][sizeOfArray],
		leafChild = new SceneGraphComponent("leaf child");
	private transient Geometry[] geoms = {new Sphere(), Primitives.coloredCube()};
	private transient Tool translateTool = new TranslateShapeTool();

	
	@Override
	public void restoreStates(Controller c) throws Exception {
		System.err.println("restoring states");
		sizeOfArray = c.getProperty(getClass(), "sizeOfArray", sizeOfArray);
		whichGeometry = c.getProperty(getClass(), "whichGeometry", whichGeometry);


		activateLeafTool = c.getProperty(getClass(), "activateLeafTool", activateLeafTool);

		minRadius = c.getProperty(getClass(), "minRadius", minRadius);
		maxRadius = c.getProperty(getClass(), "maxRadius", maxRadius);
		waveDelay = c.getProperty(getClass(), "waveDelay", waveDelay);
	}

	@Override
	public void storeStates(Controller c) throws Exception {
		System.err.println("saving states");
		c.storeProperty(getClass(), "sizeOfArray", sizeOfArray);
		c.storeProperty(getClass(), "whichGeometry", whichGeometry);

		c.storeProperty(getClass(), "activateLeafTool", activateLeafTool);

		c.storeProperty(getClass(), "minRadius", minRadius);
		c.storeProperty(getClass(), "maxRadius", maxRadius);
		c.storeProperty(getClass(), "waveDelay", waveDelay);
	}
	@Override
	public SceneGraphComponent getContent() {
		worldSGC1 = SceneGraphUtility.createFullSceneGraphComponent("Assignment 1");
		// disable display of vertices and edges
		DefaultGeometryShader dgs = ShaderUtility.createDefaultGeometryShader(worldSGC1.getAppearance(), true);

		//dgs.setShowLines(false);
		//dgs.setShowPoints(false);

		dgs.setShowLines(false);
		dgs.setShowPoints(false);

		DefaultPolygonShader dps = (DefaultPolygonShader) dgs.createPolygonShader("default");
		dps.setDiffuseColor(Color.white);

		// Set up a single scene graph component which contains the geometry for the whole scene graph.
		leafChild1.setGeometry(geoms1[whichGeometry]);

		leafChild1.setTransformation(new Transformation());
		if (activateLeafTool) leafChild1.addTool(translateTool);


		// set up the array of scene graph components
		setupArray();
		
		// have to make sure we don't let the scene graph animator set key frames on
		// our scene graph, since we animate it by hand in the method setValueAtTime() below.
		worldSGC1.getAppearance().setAttribute(SceneGraphAnimator.ANIMATED, false);
		return worldSGC1;
	}

	// set up an array of scene graph components, each having as child the leafChild instance
	// This allows us to change the geometry by changing this single instance rather than 
	// having to go through the whole array.
	protected void setupArray() {
		worldSGC1.removeAllChildren();
		DefaultGeometryShader dgs;
		DefaultPolygonShader dps;
		childArray1 = new SceneGraphComponent[sizeOfArray][sizeOfArray];
		float dstep = 1f/sizeOfArray;			// this is used to construct 
		for (int i = 0; i<sizeOfArray; ++i)	{
			for (int j = 0; j<sizeOfArray; ++j)	{
				SceneGraphComponent translated = SceneGraphUtility.createFullSceneGraphComponent("tlated"+i+j);
				SceneGraphComponent child = SceneGraphUtility.createFullSceneGraphComponent("child"+i+j);
				childArray1[i][j] = child;
				child.addChild(leafChild1);
				translated.addChild(child);
				// translate this component into an array with step size 2 
				getSetupMatrix(i, j).assignTo(translated);
				worldSGC1.addChild(translated);
				// construct a color based on the (x,y) position in the array
				// the x-value varies the red channel; the y-value varies the green channel
				// currently the appearance only has an effect when spheres are displayed!
				Color color = new Color((float) i*dstep, (float) j * dstep, .75f);
				dgs = ShaderUtility.createDefaultGeometryShader(child.getAppearance(), true);
				dps = (DefaultPolygonShader) dgs.createPolygonShader("default");
				dps.setDiffuseColor(color);
			}
		}
	}
	
	protected abstract  Matrix getSetupMatrix(int i, int j);
	//for example
//	{
//		return MatrixBuilder.euclidean().translate(2*i, 2*j, 0).getMatrix();
//	}
	/**
	 * The following method is the basic method for the animation system.  
	 * Here we create a wave of motion moving through the array of geometries
	 */
	@Override
	public void setValueAtTime(double time) {
		// TODO Auto-generated method stub
		super.setValueAtTime(time);
		for (int i = 0; i<sizeOfArray; ++i)	{
			for (int j = 0; j<sizeOfArray; ++j)	{
				setArrayValueAtTime(time, i, j).assignTo(childArray1[i][j]);
			}
		}
	}

	protected abstract Matrix setArrayValueAtTime(double time, int i, int j);
	// for example
//		// diagonal varies between 0 and 1
//		double geometryParameter = getGeometryParameter(i, j);
//		// waveParameter varies between 0 and 1
//		double waveParameter = AnimationUtility.linearInterpolation(time-waveDelay*geometryParameter, 0, 1-waveDelay, 0, 1);
//		if (waveParameter > 1) waveParameter = 1;
//		if (waveParameter < 0) waveParameter = 0;
//		double radius = inverseTent(waveParameter);
//		// change the transformation associated to this scene graph component based on the
//		// derived parameters.  
//		//	   radius determines the scaling, 
//		//	   waveParameter determines a rotation around the y-axis.
//		double angle = waveParameter*2.0*Math.PI;
////				MatrixBuilder.euclidean().rotateY(angle).scale(radius, radius, radius).assignTo(childArray[i][j]);
//		// if you've understood the above then try the following
//		double[] axis = {i,j,0};
//		return MatrixBuilder.euclidean().rotate((i==0 && j == 0) ? 0.0 : angle, axis).scale(radius, radius, .5/radius).getMatrix();
//	}

	/**
	 * Change this function so that the waves are not diagonal lines but rather concentric circles
	 * @param i
	 * @param j
	 * @return
	 */
	protected double getGeometryParameter(int i, int j) {
//		return (i+j)/(2.0*(sizeOfArray-1.0));
		double half = sizeOfArray/(2.0);
		double id = i - half, jd = j - half;
		return Math.sqrt((id*id + jd*jd)/(2*half*half));
}

	// a linear function such that f(0) = f(1) = 1.0, and f(.5) = 1.0;
	protected double inverseTent(double x)	{
		if (x < 0 || x > 1) return 1;
		double result = 2*Math.abs(x-.5);
		return AnimationUtility.linearInterpolation(result, 0.0, 1.0, minRadius, maxRadius);
	}
	
	/**
	 * This is the primary method which causes everything else to happen.  It's called 
	 * from the main() method below.
	 */
	@Override
	public void display() {
		// TODO Auto-generated method stub
		super.display();
		Viewer viewer = jrviewer.getViewer();
		CameraUtility.encompass(viewer);
		addKeyListener(viewer);
		// read in the animation file
		try {
			animationPlugin.getAnimationPanel().read(new Input(this.getClass().getResource("assg2-anim.xml")));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		animationPlugin.getAnimationPanel();
		AnimationPanel.setResourceDir("src/");
	}
	
	/**
	 * This is the file where the application properties are stored.
	 * To make sure this is updated each time you run the application,
	 * use the "Quit" menu item on the "File" menu to exit.
	 * You should implement this to reflect YOUR package structure -- don't use template!
	 */
//	@Override
//	public File getPropertyFile() {
//		return new File("src/template/assignment2.xml");
//	}

	/**
	 * Provided as an example of how to provide a key listener to an application
	 * @param viewer
	 */
	private void addKeyListener(Viewer viewer) {
		Component comp = ((Component) viewer.getViewingComponent());
		comp.addKeyListener(new KeyAdapter() {
 				public void keyPressed(KeyEvent e)	{ 
					switch(e.getKeyCode())	{
						
					case KeyEvent.VK_H:
						System.err.println("Key listener: \n\t1: toggle display of sphere");
						break;
		
					case KeyEvent.VK_1:
						whichGeometry = (whichGeometry + 1)%geoms1.length;
						leafChild1.setGeometry(geoms1[whichGeometry]);						
						break;
		
				}
 				}
			});
	}
	
	/**
	 * You have to implement this method.
	 */
//	@Override
//	public String getDocumentationFile() {
//		// TODO Auto-generated method stub
//		return  "Assignment2.html";
//	}


	@Override
	public Component getInspector() {
		// TODO Auto-generated method stub

		Box vbox = Box.createVerticalBox();

		Box vbox1 = (Box) super.getInspector();
		Box hbox = Box.createHorizontalBox();
		vbox1.add(hbox);

		JCheckBox whichGeomB = new JCheckBox("sphere");
		whichGeomB.setSelected(whichGeometry == 0);
		whichGeomB.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				boolean boo = ((JCheckBox) arg0.getSource()).isSelected();
				whichGeometry = boo ? 0 : 1;
				leafChild1.setGeometry(geoms1[whichGeometry]);						
			}
		});

		vbox1.add(whichGeomB);
		

		hbox.add(whichGeomB);
		JCheckBox leafToolB = new JCheckBox("activate tool");
		leafToolB.setSelected(activateLeafTool);
		leafToolB.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				activateLeafTool = ((JCheckBox) arg0.getSource()).isSelected();
				if (activateLeafTool) leafChild1.addTool(translateTool);
				else leafChild1.removeTool(translateTool);
			}
		});
		hbox.add(leafToolB);
				

		TextSlider sizeSlider = new TextSlider.Integer("size", SwingConstants.HORIZONTAL, 2, 40, sizeOfArray);
		sizeSlider.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				sizeOfArray = ((TextSlider)e.getSource()).getValue().intValue();
				contentPlugin.setContent(getContent());
			}
		});
		vbox1.add(sizeSlider);
		TextSlider minRSlider = new TextSlider.Double("min radius", SwingConstants.HORIZONTAL, 0.05, 1.0, minRadius);
		minRSlider.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				minRadius = ((TextSlider)e.getSource()).getValue().doubleValue();
			}
		});
		vbox1.add(minRSlider);
		TextSlider maxRSlider = new TextSlider.Double("max radius", SwingConstants.HORIZONTAL,0.05, 1.0, maxRadius);
		maxRSlider.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				maxRadius = ((TextSlider)e.getSource()).getValue().doubleValue();
			}
		});
		vbox1.add(maxRSlider);
		TextSlider wavePSlider = new TextSlider.Double("wave delay", SwingConstants.HORIZONTAL,0.0,.99, waveDelay);
		wavePSlider.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				waveDelay = ((TextSlider)e.getSource()).getValue().doubleValue();
			}
		});
		vbox1.add(wavePSlider);
		return vbox1;
	}

}
