package template.assignment2;

import charlesgunn.anim.util.AnimationUtility;
import de.jreality.math.Matrix;
import de.jreality.math.MatrixBuilder;

public class Assignment2Example extends Assignment2 {

	@Override
	protected Matrix getSetupMatrix(int i, int j) {
		// diagonal varies between 0 and 1
		double geometryParameter = getGeometryParameter(i, j);
		// waveParameter varies between 0 and 1
		double waveParameter = AnimationUtility.linearInterpolation(-waveDelay*geometryParameter, 0, 1-waveDelay, 0, 1);
		if (waveParameter > 1) waveParameter = 1;
		if (waveParameter < 0) waveParameter = 0;
		double radius = inverseTent(waveParameter);
		// change the transformation associated to this scene graph component based on the
		// derived parameters.  
		//	   radius determines the scaling, 
		//	   waveParameter determines a rotation around the y-axis.
		double angle = waveParameter*2.0*Math.PI;
//				MatrixBuilder.euclidean().rotateY(angle).scale(radius, radius, radius).assignTo(childArray[i][j]);
		// if you've understood the above then try the following
		double[] axis = {i,j,0};
		return MatrixBuilder.euclidean().rotate((i==0 && j == 0) ? 0.0 : angle, axis).scale(radius, radius, .5/radius).getMatrix();
	}

	@Override
	protected Matrix setArrayValueAtTime(double time, int i, int j) {
			return MatrixBuilder.euclidean().translate(2*i, 2*j, 0).getMatrix();
	}

	@Override
	public String getDocumentationFile() {
		// TODO Auto-generated method stub
		return "html/Assignment2.html";
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new Assignment2Example().display();
	}

}
