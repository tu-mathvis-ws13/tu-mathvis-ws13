package template.util;

import java.awt.Color;
import java.util.List;

import template.Assignment;
import charlesgunn.anim.util.AnimationUtility;
import de.jreality.geometry.IndexedFaceSetFactory;
import de.jreality.geometry.IndexedLineSetFactory;
import de.jreality.geometry.IndexedLineSetUtility;
import de.jreality.geometry.Primitives;
import de.jreality.math.MatrixBuilder;
import de.jreality.math.Pn;
import de.jreality.math.Rn;
import de.jreality.plugin.JRViewer;
import de.jreality.plugin.basic.InfoOverlayPlugin;
import de.jreality.scene.Appearance;
import de.jreality.scene.IndexedFaceSet;
import de.jreality.scene.Scene;
import de.jreality.scene.SceneGraphComponent;
import de.jreality.scene.data.Attribute;
import de.jreality.scene.data.StorageModel;
import de.jreality.scene.pick.PickResult;
import de.jreality.scene.tool.AbstractTool;
import de.jreality.scene.tool.InputSlot;
import de.jreality.scene.tool.ToolContext;
import de.jreality.shader.DefaultGeometryShader;
import de.jreality.shader.DefaultLineShader;
import de.jreality.shader.DefaultPolygonShader;
import de.jreality.shader.ShaderUtility;
import de.jreality.tutorial.app.AnimatedTextureExample;
import de.jreality.util.CameraUtility;
import de.jreality.util.PickUtility;

/**
 * I adapted the tutorial program {@link AnimatedTextureExample} in  the following way:
 * 	I added a key listener to allow the user to:
 * 		restart the game of life simulation ('1' ), and
 * 		choose between a square or torus as the display geometry ('2')
 * 		display on-line documentation in browser ('3')
 * 
 * Assignment2:
 * @author Charles Gunn
 *
 */
public class SimpleCurveEditor extends Assignment {
	
	transient SceneGraphComponent worldSGC, curveSGC, squareSGC;
	transient IndexedLineSetFactory ilsf = new IndexedLineSetFactory();
	
	public static void main(String[] args)		{
		SimpleCurveEditor theProgram = new SimpleCurveEditor();
		theProgram.display();
	}

	@Override
	public SceneGraphComponent getContent() {
		if (worldSGC != null) return worldSGC;
		worldSGC = new SceneGraphComponent("Assignment 7");
		curveSGC = new SceneGraphComponent("curve");
		squareSGC = new SceneGraphComponent("square");
		worldSGC.addChildren(curveSGC, squareSGC);
		Appearance ap = new Appearance();
		worldSGC.setAppearance(ap);
		DefaultGeometryShader dgs = ShaderUtility.createDefaultGeometryShader(ap, true);
		dgs.setShowLines(true);
		dgs.setShowPoints(true);
		DefaultPolygonShader dps = (DefaultPolygonShader) dgs.createPolygonShader("default");
		dps.setDiffuseColor(Color.white);
		DefaultLineShader dls = (DefaultLineShader) dgs.createLineShader("default");
		dls.setTubeDraw(false);
		
		ap = new Appearance();
		squareSGC.setAppearance(ap);
		dgs = ShaderUtility.createDefaultGeometryShader(ap, true);
		dgs.setShowLines(false);
		dgs.setShowPoints(false);

		ilsf = IndexedLineSetUtility.circleFactory(6, 0, 0, 1);
		// set up the texture for the Game of Life
		IndexedFaceSetFactory squareFactory = Primitives.texturedQuadrilateralFactory(null);
		IndexedFaceSet square = squareFactory.getIndexedFaceSet();
		squareSGC.setGeometry(square);
		MatrixBuilder.euclidean().scale(4.0).translate(-.5,-.5,0).assignTo(squareSGC);
		// we need non-default texture coordinates to get the image right-side-up
		curveSGC.setGeometry(ilsf.getGeometry());
		MatrixBuilder.euclidean().translate(0,0,.01).assignTo(curveSGC);
		
		curveSGC.addTool(new AbstractTool(InputSlot.LEFT_BUTTON, InputSlot.SHIFT_LEFT_BUTTON) {
			
			boolean active = false,		// a security flag to avoid editing in an inconsistent state
					deleting = false;
			double[][] curveVerts;
			int toDelete;
			@Override
			public void activate(ToolContext tc) {
				super.activate(tc);
				// usually it's sufficient to get the closest hit using tc.getCurrentPick()
				// but in this case, we always want to choose the closest hit on a vertex if there is such a hit;
				// otherwise we accept the hit on the closest edge
				List<PickResult> hitlist = tc.getCurrentPicks();
				if (hitlist == null || hitlist.size() == 0) return;
				PickResult pr = hitlist.get(0);
				// check to see if a vertex was hit; otherwise use the top pick
				for (PickResult foo : hitlist)	{
					if (pr != foo && foo.getPickType() == PickResult.PICK_TYPE_POINT) {
						System.err.println("Using point hit although edge was higher");
						pr = foo;
						break;
					}
				}
				if (pr == null || pr.getPickPath() == null || 
						pr.getPickPath().getLastElement() != ilsf.getGeometry()) return;
				
				if (tc.getSource() == InputSlot.LEFT_BUTTON)
					System.err.println("source is left button");
				if (tc.getSource() == InputSlot.SHIFT_LEFT_BUTTON) {
					System.err.println("source is shift-left button");
					if (pr.getPickType() == PickResult.PICK_TYPE_POINT) {
						deleting = true;
						toDelete = pr.getIndex();						
					}
				}
				// we have a valid hit; proceed with the algorithm
				active = true;
				// to receive events as the mouse moves need to add following input slot
				// then perform() will be called
				addCurrentSlot(InputSlot.POINTER_TRANSFORMATION);	

				curveVerts = ilsf.getIndexedLineSet().getVertexAttributes(Attribute.COORDINATES).toDoubleArrayArray(null);
				double[] newPoint = pr.getObjectCoordinates();
				// have to pay attention to whether we are working with homogeneous coordinates or not
				// *** The pick system always uses homogeneous coordinates to report pick coordinates ***
				final int fiber = curveVerts[0].length;
				if (fiber == 3)	{
					Pn.dehomogenize(newPoint, newPoint);
					// furthermore, we want to force the z-coordinate to be 0 since we know our curve lies in the z=0 plane
					// the actual pick point may have non-zero z-coordinate since it comes from the little sphere
					// representing the vertex
					newPoint = new double[]{newPoint[0], newPoint[1], 0};
				} 	else newPoint[2] = 0.0;
				// first handle the case that the user has clicked on an edge
				// here we insert a new vertex into the curve at that point
				// further picks in this activate/deactivate cycle will involve dragging this new point around
				if (pr.getPickType() == PickResult.PICK_TYPE_LINE)	{
					System.err.println("picked edge "+pr.getIndex());
					final int[][] edges = ilsf.getIndexedLineSet().getEdgeAttributes(Attribute.INDICES).toIntArrayArray(null);
					int which = pr.getIndex(), 			// which edge component?
							start = pr.getSecondaryIndex();		// this is the index of the segment within this edge component 
					int i0 = edges[which][start], i1 = edges[which][start+1];
					double[] v0 = curveVerts[i0], v1 = curveVerts[i1];
					double d0 = 0, d1 = 0;
					// calculate the distances from the picked point to the two endpoints of the picked segment
					if (fiber == 3)	{
						d0 = Rn.euclideanDistance(newPoint, v0);
						d1 = Rn.euclideanDistance(newPoint, v1);
					} else {
						d0 = Pn.distanceBetween(newPoint, v0, Pn.EUCLIDEAN);
						d1 = Pn.distanceBetween(newPoint, v1, Pn.EUCLIDEAN);
					}
					// express this as a ratio
					double f = d0/(d0+d1);
					// calculate the exact linear combination of the two endpoints based on this ratio
					double[] iv = AnimationUtility.linearInterpolation(f, 0, 1, v0, v1);
					// now add the new point to the list of vertices
					int n = curveVerts.length;
					final double[][] newVerts = new double[n+1][];
					for (int i = 0; i<n; ++i)	{
						newVerts[i] = curveVerts[i];
					}
					newVerts[n] = newPoint;
					// and insert a new index into the index list at  the correct position
					int[] newIndices = new int[edges[which].length+1];
					for (int i = 0; i<newIndices.length-1; ++i)	{
						if (i<=start) newIndices[i] = edges[which][i];
						else if (i== start+1) newIndices[i] = n;
						else newIndices[i] = edges[which][i-1];
					}
					edges[which] = newIndices;
					// edit the curve to reflect the new vertex,  using the utility method provided by the Scene class
					Scene.executeWriter(curveSGC, new Runnable() {
						@Override
						public void run() {
							ilsf.getIndexedLineSet().setVertexCountAndAttributes(Attribute.COORDINATES, StorageModel.DOUBLE_ARRAY.array(fiber).createReadOnly(newVerts));
							ilsf.getIndexedLineSet().setEdgeAttributes(Attribute.INDICES, StorageModel.INT_ARRAY_ARRAY.createReadOnly(edges));
						}
					});
				} else if (pr.getPickType() == PickResult.PICK_TYPE_POINT) {
					perform(tc);
				}
				// for perform() we only allow picking of vertices
				// notice that we turn back on picking of edges in deactivate()
				PickUtility.setPickable(curveSGC, true, false, false);
			}

			@Override
			public void perform(ToolContext tc) {
				if (!active || deleting) return;
				super.perform(tc);			// could be omitted since it's an empty method, but perhaps in the future ...
				PickResult pr = tc.getCurrentPick();
				// check that we have a valid pick
				if (pr == null || pr.getPickPath() == null || pr.getPickPath().getLastElement() != ilsf.getGeometry()) return;
				// just to be safe, check that it's a point pick
				if (pr.getPickType() == PickResult.PICK_TYPE_POINT) {
					System.err.println("Picked vertex "+pr.getIndex());
					double[] newPoint = pr.getObjectCoordinates();
					curveVerts = ilsf.getIndexedLineSet().getVertexAttributes(Attribute.COORDINATES).toDoubleArrayArray(null);
					if (curveVerts[0].length == 3)	{
						Pn.dehomogenize(newPoint, newPoint);
						newPoint = new double[]{newPoint[0], newPoint[1], newPoint[2]};
					} 	
					// force the z-coordinate to be 0
					newPoint[2] = 0.0;	
					// edit the coordinates of the picked point 
					// if there are fixed points first read the value at pr.getIndex() 
					// and return if it's value is within epsilon of a fixed point.
					curveVerts[pr.getIndex()] = newPoint;
					// and write the new results back into the IndexedLineSet
					Scene.executeWriter(curveSGC, new Runnable() {
						
						@Override
						public void run() {
							ilsf.getIndexedLineSet().setVertexCountAndAttributes(Attribute.COORDINATES, StorageModel.DOUBLE_ARRAY.array(curveVerts[0].length).createReadOnly(curveVerts));
						}
					});
				}
			}

			@Override
			public void deactivate(ToolContext tc) {
				super.deactivate(tc);
				// restore default states
				removeCurrentSlot(InputSlot.POINTER_TRANSFORMATION);
				PickUtility.setPickable(curveSGC, true, true, false);
				active = false;
				if (deleting) {
					// delete
					Scene.executeWriter(curveSGC, new Runnable() {
						@Override
						public void run() {
							IndexedLineSetUtility.removeVertex(ilsf, toDelete);
						}
					});
					System.err.println("deleting");
					deleting = false;
				}
			}
			

		});
		return worldSGC;
	}

	@Override
	public void display() {
		// TODO Auto-generated method stub
		super.display();
		CameraUtility.encompass(jrviewer.getViewer());
	}


	@Override
	public void setupJRViewer(JRViewer v)	{
		super.setupJRViewer(v);
		v.registerPlugin(new InfoOverlayPlugin());
	}

	@Override
	public String getDocumentationFile() {
		// TODO Auto-generated method stub
		return null;
	}
	
	

}
