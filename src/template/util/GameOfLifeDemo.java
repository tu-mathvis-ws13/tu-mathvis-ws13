package template.util;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.Timer;

import template.Assignment;
import de.jreality.geometry.IndexedFaceSetFactory;
import de.jreality.geometry.Primitives;
import de.jreality.math.MatrixBuilder;
import de.jreality.plugin.JRViewer;
import de.jreality.plugin.basic.InfoOverlayPlugin;
import de.jreality.scene.Appearance;
import de.jreality.scene.IndexedFaceSet;
import de.jreality.scene.SceneGraphComponent;
import de.jreality.scene.Viewer;
import de.jreality.shader.DefaultGeometryShader;
import de.jreality.shader.DefaultPolygonShader;
import de.jreality.shader.ImageData;
import de.jreality.shader.ShaderUtility;
import de.jreality.shader.Texture2D;
import de.jreality.shader.TextureUtility;
import de.jreality.tutorial.app.AnimatedTextureExample;
import de.jreality.tutorial.util.GameOfLife;
import de.jreality.util.CameraUtility;
import de.jtem.discretegroup.util.TextSlider;
import de.jtem.jrworkspace.plugin.Controller;

/**
 * I adapted the tutorial program {@link AnimatedTextureExample} in  the following way:
 * 	I added a key listener to allow the user to:
 * 		restart the game of life simulation ('1' ), and
 * 		choose between a square or torus as the display geometry ('2')
 * 		display on-line documentation in browser ('3')
 * 
 * Assignment2:
 * @author Charles Gunn
 *
 */
public class GameOfLifeDemo extends Assignment {
	
	boolean showSquare = true;
	int delay = 20;	//milliseconds
	transient int count = 0;
	transient GameOfLife gameOfLife;
	transient IndexedFaceSet torus = Primitives.torus(1.0, .5, 50, 50), 
			square = Primitives.texturedQuadrilateral();
	transient SceneGraphComponent worldSGC;
	transient private Timer timer;
	private JCheckBox geomCB;
	private TextSlider.Integer delaySlider;

	public static void main(String[] args)		{
		GameOfLifeDemo theProgram = new GameOfLifeDemo();
		theProgram.display();
	}

	@Override
	public SceneGraphComponent getContent() {
		if (worldSGC != null) return worldSGC;
		worldSGC = new SceneGraphComponent("Assignment 2");
		Appearance ap = new Appearance();
		worldSGC.setAppearance(ap);
		DefaultGeometryShader dgs = ShaderUtility.createDefaultGeometryShader(ap, true);
		dgs.setShowLines(false);
		dgs.setShowPoints(false);
		DefaultPolygonShader dps = (DefaultPolygonShader) dgs.createPolygonShader("default");
		dps.setDiffuseColor(Color.white);
		
		// set up the texture for the Game of Life
		IndexedFaceSetFactory squareFactory = Primitives.texturedQuadrilateralFactory(null);
		square = squareFactory.getIndexedFaceSet();
		// we need non-default texture coordinates to get the image right-side-up
		squareFactory.setVertexTextureCoordinates(new double[] { 0,1,1,1,1,0,0,0});
		squareFactory.update();
		final int width = 128, height = width;
		ImageData helpImage;
		Texture2D tex2d;
		BufferedImage lifeBoardImage;
		ImageData lifeBoardID;
		
		lifeBoardImage = new BufferedImage(width, height, BufferedImage.TYPE_4BYTE_ABGR);
		lifeBoardID = new ImageData(lifeBoardImage);
		tex2d = TextureUtility.createTexture(ap, "polygonShader", lifeBoardID);
		lifeBoardImage = (BufferedImage) lifeBoardID.getImage();
		tex2d.setRepeatS(Texture2D.GL_REPEAT);
		tex2d.setRepeatT(Texture2D.GL_REPEAT);
		tex2d.setMagFilter(Texture2D.GL_NEAREST);
		tex2d.setMinFilter(Texture2D.GL_NEAREST);
 		tex2d.setAnimated(true);
 		tex2d.setMipmapMode(false);
 		final Graphics2D g = lifeBoardImage.createGraphics();
		gameOfLife = new GameOfLife(lifeBoardImage);

		tex2d.setRunnable(new Runnable() {
 		
			public void run() {
				gameOfLife.update();
				Image current = gameOfLife.currentValue();
				g.drawImage(current, 0, 0, null);
			}
 			
 		});
		
		return worldSGC;
	}

	
	@Override
	public String getDocumentationFile() {
		return  "html/Assignment2.html";
	}

	@Override
	public void setupJRViewer(JRViewer v)	{
		super.setupJRViewer(v);
		v.registerPlugin(new InfoOverlayPlugin());
		v.setPropertiesFile("Assignment2.xml");
	}
	
	@Override
	public void display() {
		super.display();
		final Viewer viewer = jrviewer.getViewer();
		updateGeometry();
		if (viewer != null) CameraUtility.encompass(viewer);
		Component comp = ((Component) jrviewer.getViewer().getViewingComponent());
		comp.addKeyListener(new KeyAdapter() {
 				public void keyPressed(KeyEvent e)	{ 
					switch(e.getKeyCode())	{
						
					case KeyEvent.VK_1:
						gameOfLife.resetBoard();
						break;
		
					case KeyEvent.VK_2:
						showSquare = !showSquare;
						updateGeometry();
						break;
				
				}
 				}
			});
		timer = new Timer(delay, new ActionListener()	{

			public void actionPerformed(ActionEvent e) {
				viewer.renderAsync();		
			}
 			
 		});
		timer.start();
	}

	@Override
	public Component getInspector() {
		delaySlider = new TextSlider.Integer("delay",SwingConstants.HORIZONTAL, 1, 1000, delay);
		delaySlider.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				delay =  ((TextSlider) arg0.getSource()).getValue().intValue();
				timer.setDelay(delay);
			}
		});
		delaySlider.setToolTipText("Set the delay in milliseconds per update");
		JButton resetb = new JButton("Reset");
		resetb.setToolTipText("Generate new initial condition");
		resetb.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent actionevent) {
				gameOfLife.resetBoard();
			}
		});
		geomCB = new JCheckBox("Show torus");
		geomCB.setSelected(!showSquare);
		geomCB.setToolTipText("Toggle square and 3D embedded torus");
		geomCB.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent actionevent) {
				showSquare = !((JCheckBox)actionevent.getSource()).isSelected();
				updateGeometry();
			}
		});
		
		Insets insets = new Insets(5,0,5,0);
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.weightx = .50;
		c.insets = insets;
		c.weighty = 0.0;
		c.insets = insets;
		c.anchor = GridBagConstraints.WEST;

		JPanel panel = new JPanel();
		panel.setLayout(new GridBagLayout());
		c.weightx = 0.0;
		c.gridx = c.gridy = 0;
		panel.add(resetb, c);
		c.weightx = 1.0;
		c.gridx = 1;
		panel.add(geomCB, c);

		c.gridwidth = 2;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 1;
		c.weightx = 1.0;
		panel.add(delaySlider, c);
		
		return panel;
	}
	
	protected void updateGeometry() {
		worldSGC.setGeometry(showSquare ? square : torus);
		MatrixBuilder.euclidean().assignTo(worldSGC);
		if (jrviewer != null) CameraUtility.encompass(jrviewer.getViewer());
	}

	@Override
	public void restoreStates(Controller c) throws Exception {
		delay = c.getProperty(getClass(), "delay", delay);
		if (delaySlider != null) delaySlider.setValue(delay);
		showSquare = c.getProperty(getClass(), "showSquare", showSquare);
		if (geomCB != null) geomCB.setSelected(!showSquare);
		System.err.println("restored states = "+delay+" "+showSquare);
	}

	@Override
	public void storeStates(Controller c) throws Exception {
		System.err.println("Storing "+delay+" "+showSquare);
		c.storeProperty(getClass(), "delay", delay);
		c.storeProperty(getClass(), "showSquare", showSquare);
	}

	@Override
	public File getPropertyFile() {
		return new File("src/template/util/gameoflife.xml");
	}
	

}
