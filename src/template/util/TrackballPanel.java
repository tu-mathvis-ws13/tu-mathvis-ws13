package template.util;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.SwingConstants;

import de.jreality.geometry.TubeFactory;
import de.jreality.math.Matrix;
import de.jreality.math.Rn;
import de.jreality.plugin.JRViewer;
import de.jreality.plugin.JRViewer.ContentType;
import de.jreality.scene.SceneGraphComponent;
import de.jreality.scene.SceneGraphPath;
import de.jreality.scene.Transformation;
import de.jreality.scene.event.TransformationEvent;
import de.jreality.scene.event.TransformationListener;
import de.jreality.scene.tool.Tool;
import de.jreality.tools.DraggingTool;
import de.jreality.tools.RotateTool;
import de.jreality.tools.TrackballRotateTool;
import de.jreality.ui.viewerapp.ViewerApp;

public class TrackballPanel {

	Component comp = null;
	private SceneGraphComponent sgc;
	private SceneGraphComponent axes;
 	
	public TrackballPanel()	{
		sgc = new SceneGraphComponent();
		axes = TubeFactory.getXYZAxes();
		sgc.addChild(axes);
		TrackballRotateTool trt = new TrackballRotateTool();
		axes.addChild(trt.getTrackball());
		axes.addTool(trt);
		axes.setTransformation(new Transformation());
		axes.getTransformation().addTransformationListener(new TransformationListener()	{
			
			double[] themat = new double[16];
			public void transformationMatrixChanged(TransformationEvent ev) {
				ev.getMatrix(themat);
				Matrix m = new Matrix(themat);
				System.err.println("current matrix is "+Rn.matrixToString(m.getArray()));
			}

			
		});

       JRViewer jrv = new JRViewer();
       jrv.addContentSupport(ContentType.Raw);
       jrv.setContent(sgc);
       jrv.encompassEuclidean();
       jrv.startupLocal();
//       jrv.getViewer().getSceneRoot().getAppearance().setAttribute("backgroundColor", Color.red);
       comp = (Component) jrv.getViewer().getViewingComponent();
       JPanel jpanel = new JPanel();
       jpanel.setLayout(new BorderLayout());
       jpanel.add(comp); //, SwingConstants.CENTER);
       comp = jpanel;
	   comp.setMinimumSize(new Dimension(220, 300));
       comp.setPreferredSize(new Dimension(300, 300));
       comp.setMaximumSize(new java.awt.Dimension(32768,32768));
 	}
	public Component getComponent()	{
		return comp;
	}
	
	public SceneGraphComponent getSceneGraphComponent() {
		return axes;
	}

}
