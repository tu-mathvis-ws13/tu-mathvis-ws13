package template.util;

import java.awt.Component;

import de.jreality.geometry.Primitives;
import de.jreality.scene.SceneGraphComponent;
import de.jreality.scene.event.TransformationEvent;
import de.jreality.scene.event.TransformationListener;
import de.jreality.util.CameraUtility;
import de.jreality.util.SceneGraphUtility;
import template.Assignment;

public class TestTrackballPanel extends Assignment {

	TrackballPanel tp = new TrackballPanel();
	@Override
	public SceneGraphComponent getContent() {
		final SceneGraphComponent world = SceneGraphUtility.createFullSceneGraphComponent("world");
		world.setGeometry(Primitives.coloredCube());
		tp.getSceneGraphComponent().getTransformation().addTransformationListener(new TransformationListener() {
			
			@Override
			public void transformationMatrixChanged(TransformationEvent ev) {
				world.getTransformation().setMatrix(ev.getTransformationMatrix());
			}
		});
		return world;
	}

	@Override
	public String getDocumentationFile() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Component getInspector() {
		// TODO Auto-generated method stub
		super.getInspector();
		inspector.add(tp.getComponent());
		return inspector;
	}

	@Override
	public void display() {
		// TODO Auto-generated method stub
		super.display();
		CameraUtility.encompass(jrviewer.getViewer());
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new TestTrackballPanel().display();
	}

}
