package template.util;

import de.jreality.math.P3;
import de.jreality.math.Pn;
import de.jreality.math.Rn;
import de.jtem.projgeom.PlueckerLineGeometry;

public class UtilityMethods {

	/**
	 * In P3, find the orthographic projection of a point onto a plane.
	 * @param dst		the projected point
	 * @param pt		the point to be projected
	 * @param plane		the plane on which is projected
	 * @param metric	the metric to use
	 * @return
	 */
	public static double[] projectPointOntoPlane(double[] dst, double[] pt, double[] plane, int metric)	{
		double[] perpLine = lineThruPointPerpToPlane(null, pt, plane, metric);
		dst = PlueckerLineGeometry.lineIntersectPlane(dst, perpLine, plane);
		Pn.setToLength(dst, dst, 1.0, metric);
		return dst;
	}
	
	/**
	 * In P3 find the line (in Pluecker line coords) that passes through a given point and 
	 * is perpendicular to a given plane.  Not well defined if the point is already perpendicular
	 * to the line.
	 * @param dst
	 * @param pt
	 * @param plane
	 * @param metric
	 * @return
	 */
	public static double[] lineThruPointPerpToPlane(double[] dst, double[] pt, double[] plane, int metric)	{
		double[] polarPt = Pn.polarize(null, plane, metric);
		return PlueckerLineGeometry.lineFromPoints(null, pt, polarPt);
	}

	public static void main(String[] args) {
		double[][] tt =  
			{{.5, .5, .5},{1,-1,-1},{-1,1,-1},{-1,-1,1}};
		tt = Pn.homogenize(null, tt);
		double[] plane = P3.planeFromPoints(null, tt[1], tt[2], tt[3]);
		double[] point = projectPointOntoPlane(null, tt[0], plane, Pn.ELLIPTIC);
		System.err.println("point = "+Rn.toString(point));
		System.err.println("<pt, pl> = "+Rn.innerProduct(plane, point));

	}
}
