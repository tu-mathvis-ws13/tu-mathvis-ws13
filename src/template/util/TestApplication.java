package template.util;

import de.jreality.geometry.Primitives;
import de.jreality.plugin.JRViewer;
import de.jreality.plugin.basic.Inspector;
import de.jreality.scene.IndexedFaceSet;
import de.jreality.ui.viewerapp.Navigator;
import de.jreality.util.CameraUtility;
import de.jtem.halfedgetools.adapter.AdapterSet;
import de.jtem.halfedgetools.jreality.ConverterJR2Heds;
import de.jtem.halfedgetools.plugin.HalfedgeInterface;
import de.jtem.halfedgetools.plugin.VectorFieldManager;
import de.jtem.halfedgetools.tutorial.TestAlgorithm;
import de.jtem.halfedgetools.tutorial.TestPlugin;
import de.jtem.halfedgetools.tutorial.TestPositionAdapter;
import de.jtem.halfedgetools.tutorial.TestScalarFunction;
import de.jtem.halfedgetools.tutorial.TestVisualizer;
import de.jtem.halfedgetools.tutorial.VE;
import de.jtem.halfedgetools.tutorial.VF;
import de.jtem.halfedgetools.tutorial.VHDS;
import de.jtem.halfedgetools.tutorial.VV;

public class TestApplication {

	public static void main(String[] args) {
		
		// Halfedge konkret
		VHDS vhds = new VHDS();
//		VV v = vhds.addNewVertex();
//		VE e = vhds.addNewEdge();
//		VE e2 = vhds.addNewEdge();
//		VF f = vhds.addNewFace();
//		e.setTargetVertex(v);
//		e2.setTargetVertex(v);
//		e.linkOppositeEdge(e2);
//		e.linkNextEdge(e2);
//		e.setLeftFace(f);
		
		// Das Adapter Konzept
		TestPositionAdapter pa = new TestPositionAdapter();
		
		// Das Adapter Set
		AdapterSet a = new AdapterSet(pa);
		
		// Die generischen Adapter
		a.addAll(AdapterSet.createGenericAdapters());

		// Ein Beispiel Algorithmus
		TestAlgorithm.doSomething(vhds, a);
		
		// Beispiel Applikation mit Plugin
		JRViewer jv = new JRViewer();
		jv.addContentUI();
		jv.registerPlugin(TestPlugin.class);
		jv.registerPlugin(VectorFieldManager.class);
		jv.registerPlugin(TestVisualizer.class);
		jv.registerPlugin(HalfedgeInterface.class);
		jv.registerPlugin(Inspector.class);
		jv.startup();
		
		// Load some geometry
		IndexedFaceSet icosa = Primitives.icosahedron();
		ConverterJR2Heds converter = new ConverterJR2Heds();
		converter.ifs2heds(icosa, vhds, a);

		HalfedgeInterface hif = jv.getPlugin(HalfedgeInterface.class);
		hif.addAdapter(new TestScalarFunction(), true);
		hif.set(vhds);
		CameraUtility.encompass(jv.getViewer());
	}
	
}
