package template.assignment5;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import charlesgunn.jreality.geometry.projective.CircleFactory;
import charlesgunn.jreality.geometry.projective.SurfaceElement;

import template.Assignment;
import de.jreality.math.P3;
import de.jreality.math.Pn;
import de.jreality.plugin.JRViewer;
import de.jreality.plugin.JRViewer.ContentType;
import de.jreality.scene.Appearance;
import de.jreality.scene.Geometry;
import de.jreality.scene.IndexedFaceSet;
import de.jreality.scene.SceneGraphComponent;
import de.jreality.scene.Transformation;
import de.jreality.shader.CommonAttributes;
import de.jreality.util.SceneGraphUtility;
import de.jtem.discretegroup.core.DiscreteGroupElement;
import de.jtem.discretegroup.core.DiscreteGroupSceneGraphRepresentation;
import de.jtem.discretegroup.groups.TriangleGroup;
import de.jtem.discretegroup.util.TextSlider;
/**
 * This class is a "Christmas tree decoration" appplication.
 * 
 * It allows the user to choose a spherical group based on one of the triangle groups *233, *234, or *235, and
 * design a Christmas tree decoration based on this symmetry group.
 * 
 * If you want to add your own design, you need only to implement the interface {@link FundamentalDomainAdaptor}
 * and add it to the list of adaptors in {@link Assignment5}.  You are allowed to edit Assignment5 in this case.
 * 
 * The three adaptors which are provided are arranged in a super-class ordering:
 * 	{@link DoesNothingAdaptor} simply returns the default fundamental domain for the group
 * {@link PerpendicularCutAdaptor} is a subclass of {@link DoesNothingAdaptor} and adjusts the fundamental triangle
 * so that the resulting polygon determines a platonic or archimedean solid.
 * {@link TwirlyAdaptor} subclasses {@link PerpendicularCutAdaptor} to construct -- on top of its fundamental domain -- 
 * a rotated, extruded form.
 * 
 * @author gunn
 *
 */
public class Assignment5 extends Assignment {

	protected int whichAdaptor = 0;
	protected int whichGroup = 2;
	protected boolean showOnlyOne = false,
			showMirrors = true,
			vrSupport = true;
	
	transient protected SceneGraphComponent worldSGC  = SceneGraphUtility.createFullSceneGraphComponent("world"), 
			fundamentalDomain = SceneGraphUtility.createFullSceneGraphComponent("fd"),
			processedTriangleSGC = SceneGraphUtility.createFullSceneGraphComponent("process triangle"),
			contentHolder = new SceneGraphComponent("holder"),
			mirrorRepnSGC = SceneGraphUtility.createFullSceneGraphComponent("process triangle"),
			mirrorsSGC[] = new SceneGraphComponent[3];
	transient protected FundamentalDomainAdaptor[] adaptorSet = {new DoesNothingAdaptor(), new PerpendicularCutAdaptor(),  new TwirlyAdaptor()};  // DoesNothingAdapter();
	transient TriangleGroup theGroup;
	transient DiscreteGroupSceneGraphRepresentation dgsgr;
	static String[] names = {"233","*233","234","*234","3*2","235","*235"};

		
	@Override
	public SceneGraphComponent getContent() {
		SurfaceElement.setDiskRadius(1.01);
		for (int i = 0; i<3; ++i)	{
			mirrorsSGC[i] = new SceneGraphComponent("mirror"+i);
			mirrorsSGC[i].setTransformation(new Transformation());
			mirrorRepnSGC.addChild(mirrorsSGC[i]);
		}
		update();
		fundamentalDomain.addChild(processedTriangleSGC);
		mirrorRepnSGC.getAppearance().setAttribute(CommonAttributes.VERTEX_DRAW, false);
		mirrorRepnSGC.getAppearance().setAttribute(CommonAttributes.TRANSPARENCY, false);
		fundamentalDomain.getAppearance().setAttribute("polygonShader.diffuseColor", Color.white);
		fundamentalDomain.getAppearance().setAttribute("lineShader.diffuseColor", Color.yellow);
		fundamentalDomain.getAppearance().setAttribute("pointShader.diffuseColor", Color.cyan);
		return contentHolder;
	}

	@Override
	public void setupJRViewer(JRViewer v) {
		super.setupJRViewer(v);
		if (vrSupport) {
			v.addVRSupport();
		}
		v.addContentUI();
		v.addContentSupport(ContentType.Raw);
	}

	static DiscreteGroupElement[] idonly = {new DiscreteGroupElement()};
	private void update() {
		theGroup = TriangleGroup.instanceOfGroup(names[whichGroup]);
		theGroup.update();
		dgsgr = new DiscreteGroupSceneGraphRepresentation(theGroup);
		if (showOnlyOne) {
			dgsgr.setElementList(idonly);
		} else dgsgr.setElementList(theGroup.getElementList());
		
		FundamentalDomainAdaptor fdf = adaptorSet[whichAdaptor];
		fdf.setTriangleGroup(theGroup);
		fdf.update();
		processedTriangleSGC.removeAllChildren();
		processedTriangleSGC.addChild(fdf.getGeometrySGC());

		dgsgr.setWorldNode(fundamentalDomain);
		dgsgr.update();
		contentHolder.removeAllChildren();
		contentHolder.addChildren(getMirrorRepn(), dgsgr.getRepresentationRoot());
	}

	private SceneGraphComponent getMirrorRepn() {
		double[][] verts = theGroup.getTriangle();
		for (int i = 0; i<3; ++i)	{
			int j = (i+1)%3, k = (i+2)%3;
			mirrorsSGC[i] = SurfaceElement.surfaceElement(mirrorsSGC[i], 
					P3.originP3, 
					P3.planeFromPoints(null, P3.originP3, verts[j], verts[k]), 
					1.1, 
					Pn.EUCLIDEAN);
		}
		return mirrorRepnSGC;
	}

	@Override
	public void display() {
		super.display();
		Appearance ap = jrviewer.getViewer().getSceneRoot().getAppearance();
		ap.setAttribute(CommonAttributes.VERTEX_DRAW, true);
	}

//	@Override
//	public File getPropertyFile() {
//		return new File("src/template/assignment5/assignment5.xml");
//	}
//
	@Override
	public String getDocumentationFile() {
		return "Assignment5.html";
	}

	Component currentFDAInspector;
	Box inspector = Box.createVerticalBox();
	@Override
	public Component getInspector() {
		
		// Allow the use to choose a triangle group
		JComboBox groupscb = new JComboBox(names);
		groupscb.setSelectedIndex(whichGroup);
		groupscb.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				whichGroup = ((JComboBox) arg0.getSource()).getSelectedIndex();
				update();
			}
		});
		inspector.add(groupscb);
		Box hbox = Box.createHorizontalBox();
		inspector.add(hbox);
		hbox.add(Box.createHorizontalGlue());
		JLabel label = new JLabel("Style:");
		hbox.add(label);
		hbox.add(Box.createHorizontalGlue());
		
		// Allow the use to choose a triangle group
		String[] names = new String[adaptorSet.length];
		for (int i = 0; i<adaptorSet.length; ++i)
			names[i] = adaptorSet[i].getName();
		JComboBox styleCB = new JComboBox(names);
		styleCB.setSelectedIndex(whichAdaptor);
		styleCB.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				whichAdaptor = ((JComboBox) arg0.getSource()).getSelectedIndex();
				setWhichAdaptor(whichAdaptor);
				update();
			}
		});
		hbox.add(styleCB);
		hbox.add(Box.createHorizontalGlue());

		hbox = Box.createHorizontalBox();
		inspector.add(hbox);
		JCheckBox onlyOneCB = new JCheckBox("show 1 only");
		onlyOneCB.setSelected(showOnlyOne);
		onlyOneCB.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				showOnlyOne = ((JCheckBox) e.getSource()).isSelected();
				if (showOnlyOne) {
					dgsgr.setElementList(idonly);
				} else dgsgr.setElementList(theGroup.getElementList());
				dgsgr.update();
			}
		});
		hbox.add(Box.createHorizontalGlue());
		hbox.add(onlyOneCB);
		hbox.add(Box.createHorizontalGlue());
		JCheckBox showMirrorsCB = new JCheckBox("show mirrors");
		showMirrorsCB.setSelected(showMirrors);
		showMirrorsCB.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				showMirrors = ((JCheckBox) e.getSource()).isSelected();
				mirrorRepnSGC.setVisible(showMirrors);
			}
		});
		hbox.add(showMirrorsCB);
		hbox.add(Box.createHorizontalGlue());
		final TextSlider.Double transpSlider = new TextSlider.Double("transparency",SwingConstants.HORIZONTAL, 0,1,transparency);
		transpSlider.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				double t = ((TextSlider) arg0.getSource()).getValue().doubleValue();
				setTransparency(t);
			}
		});
		inspector.add(transpSlider);

		inspector.add(currentFDAInspector = adaptorSet[whichAdaptor].getInspector());
		return inspector;
	}
	
	protected double transparency = 0.0;
	public double getTransparency() {
		return transparency;
	}

	public void setTransparency(double transparency) {
		this.transparency = transparency;
		processedTriangleSGC.getAppearance().setAttribute("polygonShader."+CommonAttributes.TRANSPARENCY, transparency);
		processedTriangleSGC.getAppearance().setAttribute(CommonAttributes.TRANSPARENCY_ENABLED, transparency != 0.0);
	}


	public int getWhichAdaptor() {
		return whichAdaptor;
	}

	public void setWhichAdaptor(int whichAdaptor) {
		this.whichAdaptor = whichAdaptor;
		inspector.remove(currentFDAInspector);
		inspector.add(currentFDAInspector = adaptorSet[whichAdaptor].getInspector());
	}

	public static void main(String[] args) {
		new Assignment5().display();
	}
}
