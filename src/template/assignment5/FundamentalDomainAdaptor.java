package template.assignment5;

import java.awt.Component;

import de.jreality.scene.IndexedFaceSet;
import de.jreality.scene.SceneGraphComponent;
import de.jtem.discretegroup.groups.TriangleGroup;

public interface FundamentalDomainAdaptor {
	public String getName();
	public void setTriangleGroup(TriangleGroup tg);
	public SceneGraphComponent getGeometrySGC();
	public void update();
	public Component getInspector();
}
