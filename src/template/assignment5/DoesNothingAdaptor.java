package template.assignment5;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.SwingConstants;

import charlesgunn.anim.util.AnimationUtility;
import charlesgunn.jreality.geometry.projective.CurveCollector;

import de.jreality.geometry.IndexedFaceSetUtility;
import de.jreality.math.Matrix;
import de.jreality.math.MatrixBuilder;
import de.jreality.math.P3;
import de.jreality.math.Pn;
import de.jreality.math.Rn;
import de.jreality.scene.Geometry;
import de.jreality.scene.IndexedFaceSet;
import de.jreality.scene.SceneGraphComponent;
import de.jreality.scene.data.Attribute;
import de.jreality.scene.data.StorageModel;
import de.jreality.shader.CommonAttributes;
import de.jreality.util.SceneGraphUtility;
import de.jtem.discretegroup.groups.TriangleGroup;
import de.jtem.discretegroup.util.TextSlider;

public class DoesNothingAdaptor implements FundamentalDomainAdaptor {

	protected TriangleGroup theGroup = null;
	protected IndexedFaceSet originalGeom = null;
	protected int whichVertex = 0;
	protected SceneGraphComponent theSGC = SceneGraphUtility.createFullSceneGraphComponent("Fundamental domain adaptor");
	
	@Override
	public void setTriangleGroup(TriangleGroup tg)	{
		theGroup = tg;
		originalGeom = (IndexedFaceSet) tg.getDefaultFundamentalRegion();
		theSGC.setGeometry(originalGeom);
	}
	
	@Override
	public SceneGraphComponent getGeometrySGC() {
		return theSGC;
	}

	public void update() {
		
	}
	
	public int getWhichVertex() {
		return whichVertex;
	}

	public void setWhichVertex(int whichVertex) {
		this.whichVertex = whichVertex;
		update();
	}

	@Override
	public Component getInspector()	{
		Box vbox = Box.createVerticalBox();
		final TextSlider.Integer sizeSlider = new TextSlider.Integer("vertex",SwingConstants.HORIZONTAL, 0,2,whichVertex);
		sizeSlider.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				whichVertex = ((TextSlider) arg0.getSource()).getValue().intValue();
				setWhichVertex(whichVertex);
			}
		});
		vbox.add(sizeSlider);

		return vbox;
	}


	@Override
	public String getName() {
		return "Default triangle";
	}
}
