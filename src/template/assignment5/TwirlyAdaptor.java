package template.assignment5;

import java.awt.Component;

import javax.swing.Box;

import charlesgunn.anim.util.AnimationUtility;
import charlesgunn.jreality.geometry.projective.CurveCollector;
import de.jreality.geometry.IndexedFaceSetUtility;
import de.jreality.math.Matrix;
import de.jreality.math.MatrixBuilder;
import de.jreality.math.P3;
import de.jreality.math.Pn;
import de.jreality.math.Rn;
import de.jreality.scene.Appearance;
import de.jreality.scene.Geometry;
import de.jreality.scene.IndexedFaceSet;
import de.jreality.scene.data.Attribute;
import de.jreality.scene.data.StorageModel;
import de.jreality.shader.DefaultGeometryShader;
import de.jreality.shader.DefaultPolygonShader;
import de.jreality.shader.ImplodePolygonShader;
import de.jreality.shader.ShaderUtility;
import de.jreality.shader.TwoSidePolygonShader;
import de.jtem.beans.InspectorPanel;
import de.jtem.discretegroup.groups.TriangleGroup;

public class TwirlyAdaptor extends PerpendicularCutAdaptor {

	protected int numCurves = 30;
	protected double scale = .05, 
			rotate = Math.PI, 
			translate = 1;
	protected boolean powerOfSingleM = true;
	transient protected double[] centerPoint;
	transient CurveCollector cc = new CurveCollector(numCurves, 2, 4);
	
	@Override
	public void setWhichVertex(int whichVertex) {
		super.setWhichVertex(whichVertex);
		centerPoint = theGroup.getTriangle()[whichVertex].clone();
		update();
	}

	
	@Override
	public void setTriangleGroup(TriangleGroup tg) {
		super.setTriangleGroup(tg);
		centerPoint = theGroup.getTriangle()[whichVertex].clone();
		update();
	}

	@Override
	public void update() {
		super.update();
		cc = new CurveCollector(numCurves, curve.length, 4);
		
		double scaleStep = Math.exp(Math.log(scale)/numCurves),
				rotateStep = rotate/numCurves,
				translateStep = translate/numCurves;
		double[] tlate = centerPoint.clone();
		tlate[3] = 0.0;
		Matrix toOrigin = MatrixBuilder.euclidean().translate(Rn.times(null, 1, tlate)).getMatrix();
		// Calculate a single motion that scales, then rotates around an axis, then translates along the axis.
		Matrix m = MatrixBuilder.euclidean().translate(Rn.times(null, translateStep, tlate)).
				rotate(rotateStep, centerPoint).
				scale( Math.exp(Math.log(scale)/numCurves)).getMatrix();
		// the scale needs to happen in a translated coordinate system
		// the following conjugation carries this out
		double[] foo = Rn.conjugateByMatrix(null, m.getArray(), toOrigin.getArray());
		m = new Matrix(foo);
		double[][] colors = new double[curve.length*numCurves][];
		int cn = curve.length;
		double[][] baseC = {{0,1,0,1},{1,0,0,1}};
		for (int i = 0; i<numCurves; ++i)	{
			Matrix tmp = null;
			if (powerOfSingleM) {
				tmp = Matrix.power(m, i);
			} else {
				m = MatrixBuilder.euclidean().translate(Rn.times(null, i*translateStep, tlate)).
						rotate(i*rotateStep, centerPoint).
						scale( Math.exp(i*Math.log(scale)/numCurves)).getMatrix();
				foo = Rn.conjugateByMatrix(null, m.getArray(), toOrigin.getArray());
				 tmp = new Matrix(foo);				
			}
			double[][] curve2 = Rn.matrixTimesVector(null, tmp.getArray(), curve);
			cc.addCurve(curve2);
			for (int j = 0;j < cn; ++j)	
				colors[cn*i+j] = AnimationUtility.linearInterpolation(null, Math.sqrt(i), 0, Math.sqrt(numCurves-1.0), baseC[0], baseC[1]);
		}
		IndexedFaceSet mesh = cc.getMesh();
		IndexedFaceSetUtility.calculateAndSetEdgesFromFaces(mesh);
		mesh.setVertexAttributes(Attribute.COLORS, 
				StorageModel.DOUBLE_ARRAY.array().createReadOnly(colors));
		theSGC.setGeometry(mesh);

	}
	
	public int getNumCurves() {
		return numCurves;
	}
	public void setNumCurves(int numCurves) {
		this.numCurves = numCurves;
	}
	public double getScale() {
		return scale;
	}
	public void setScale(double scale) {
		this.scale = scale;
	}
	public double getRotate() {
		return rotate;
	}
	public void setRotate(double rotate) {
		this.rotate = rotate;
	}
	public double getTranslate() {
		return translate;
	}
	public void setTranslate(double translate) {
		this.translate = translate;
	}
	

	public boolean isPowerOfSingleM() {
		return powerOfSingleM;
	}


	public void setPowerOfSingleM(boolean powerOfSingleM) {
		this.powerOfSingleM = powerOfSingleM;
	}


	@Override
	public String getName() {
		return "Twirly";
	}

	@Override
	public Component getInspector() {
		Box vbox = (Box) super.getInspector();
		InspectorPanel ip = new InspectorPanel();
		ip.setObject(this);
		ip.setUpdateMethod("update");
		vbox.add(ip);
		return vbox;
	}

}

//private double[] getFrameFromTriangle(double[][] verts) {
//	double[] center = Rn.average(null, verts);
//	center[3] = 1.0;
//	double[] zdir = P3.planeFromPoints(null, verts[2], P3.originP3, center);
//	zdir[3] = 0.0;
//	double[] xdir = Rn.subtract(null, center, verts[2]);
//	double[] ydir = Rn.crossProduct(null, xdir, zdir);
//	double[] ydir4 = new double[4];
//	System.arraycopy(ydir, 0, ydir4, 0, 3);
//	Rn.normalize(xdir, xdir);
//	Rn.normalize(ydir4, ydir4);
//	Rn.normalize(zdir, zdir);
//	Matrix moo = new Matrix();
//	moo.setColumn(0, xdir);
//	moo.setColumn(1, ydir4);
//	moo.setColumn(2, zdir);
//	moo.setColumn(3, verts[2]);
//	Matrix scale =  MatrixBuilder.euclidean().scale(Pn.distanceBetween(center, verts[2], Pn.EUCLIDEAN)).getMatrix();
//	return Rn.times(null, moo.getArray(), scale.getArray());
//}
//
//private double[] getFrame2FromTriangle(double[][] verts) {
//	double[] center = Rn.average(null, verts);
//	center[3] = 1.0;
//
//	double[] zdir = P3.planeFromPoints(null, verts[0], verts[1], verts[2]);
//	zdir[3] = 0.0;
//	double[] xdir = Rn.subtract(null, verts[2], verts[0]);
//	double[] ydir = Rn.crossProduct(null, zdir, xdir);
//	double[] ydir4 = new double[4];
//	System.arraycopy(ydir, 0, ydir4, 0, 3);
//	xdir[3] = ydir4[3] = zdir[3] = 0.0;
//	Rn.normalize(xdir, xdir);
//	Rn.normalize(ydir4, ydir4);
//	Rn.normalize(zdir, zdir);
//	Matrix moo = new Matrix();
//	moo.setColumn(0, xdir);
//	moo.setColumn(1, ydir4);
//	moo.setColumn(2, zdir);
//	moo.setColumn(3, center);
//	Matrix scale = new Matrix();
////	Matrix scale = MatrixBuilder.euclidean().scale(Pn.distanceBetween(center, verts[2], Pn.EUCLIDEAN)).getMatrix();
//	return Rn.times(null, moo.getArray(), scale.getArray());
//}
//
