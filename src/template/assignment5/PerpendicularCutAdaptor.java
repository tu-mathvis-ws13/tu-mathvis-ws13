package template.assignment5;

import de.jreality.geometry.IndexedFaceSetFactory;
import de.jreality.geometry.IndexedFaceSetUtility;
import de.jreality.math.Matrix;
import de.jreality.math.MatrixBuilder;
import de.jreality.math.P3;
import de.jreality.math.Pn;
import de.jreality.math.Rn;
import de.jtem.discretegroup.groups.TriangleGroup;

public class PerpendicularCutAdaptor extends DoesNothingAdaptor {

	IndexedFaceSetFactory perpCutFactory = new IndexedFaceSetFactory();
	
	double[][] curve = new double[2][];
	
	@Override
	public void update() {
		super.update();
		double[][] verts = theGroup.getTriangle();
		if (verts.length != 3) 
			throw new IllegalArgumentException("Only can handle triangles");
		System.err.println("verts = "+Rn.toString(verts));
		// calculate plane through chosen point perpendicular to line through origin
		double[] cuttingPlane = verts[whichVertex].clone();
		cuttingPlane[3] = -Rn.innerProduct(verts[whichVertex], verts[whichVertex], 3);
		verts = verts.clone();
		for (int i = 0; i<verts.length; ++i) verts[i] = verts[i].clone();
		// find the vertex farthest from origin
		int farthest = 0;
		double farthestD = 0;
		for (int i = 0; i<3; ++i)	{
			// find intersection of line joining vi and O with cutting plane
			double f = Rn.innerProduct(verts[whichVertex], verts[i], 3);
			Rn.times(verts[i], 1/f, verts[i]);
			verts[i][3] = 1.0;
			double d = Pn.norm(verts[i], Pn.EUCLIDEAN);
			if (d > farthestD) {
				farthestD = d;
				farthest = i;
			}
		}
		// force the resulting figure to be inscribed in unit sphere
		Rn.times(verts, 1.0/farthestD, verts);
		for (int i = 0; i<verts.length; ++i) verts[i][3] = 1.0;
		
		int[][] faceIndices =  tfInd, edgeIndices = teInd;
		double[][] vertices;
		curve = new double[2][];
		if (theGroup.getName().startsWith("2")) {	
			// rotation group, need to combine two triangles together to get FD
//			double[][] tverts = new double[4][];
			for (int i = 0; i<3; ++i)
				verts[i] = verts[i].clone();
			int w = whichVertex;
			// exactly which one depends on the choice of distinguished vertex
			int secondVertex = w != 0 ? 0 : 1,
					thirdVertex = w != 0 ? (w == 1 ? 2 : 1) : 2;
			double[] reflectPlane = P3.planeFromPoints(null, verts[w], verts[secondVertex], P3.originP3);
			Matrix m = MatrixBuilder.euclidean().reflect(reflectPlane).getMatrix();
			if (w == 0)	{
				verts[0] = m.multiplyVector(verts[thirdVertex]);
				curve = new double[3][];
				curve[0] = verts[thirdVertex];
				curve[1] = verts[secondVertex];
				curve[2] = verts[0];
				vertices = verts;
			} else {
				verts[secondVertex] = m.multiplyVector(verts[thirdVertex]);
				vertices = verts;
				faceIndices = new int[][]{{secondVertex,w, thirdVertex}};
				edgeIndices = new int[][]{{secondVertex,w},{w,thirdVertex},{thirdVertex, secondVertex}};
				curve[w == 1 ? 1 : 0] = verts[thirdVertex];
				curve[w == 1 ? 0 : 1] = verts[secondVertex];
			}
		} else {
			vertices = verts;
			curve[0] = verts[(whichVertex+1)%3];
			curve[1] = verts[(whichVertex+2)%3];
		}
		perpCutFactory.setVertexCount(vertices.length);
		perpCutFactory.setVertexCoordinates(vertices);
		perpCutFactory.setFaceIndices(faceIndices);
		perpCutFactory.setEdgeCount(edgeIndices.length);
		perpCutFactory.setEdgeIndices(edgeIndices);
		perpCutFactory.update();
		theSGC.setGeometry(perpCutFactory.getIndexedFaceSet());
	}
	
	@Override
	public void setTriangleGroup(TriangleGroup tg) {
		super.setTriangleGroup(tg);
		perpCutFactory = getDefaultFundamentalRegionFactory(theGroup);
		perpCutFactory.setGenerateFaceNormals(true);
		perpCutFactory.setGenerateVertexNormals(true);
	}
	
	

	@Override
	public String getName() {
		return "Perpendicular cut";
	}



	protected static final int[][] teInd = new int[][]{{0,1},{1,2},{2,0}},
			tfInd = new int[][]{{0,1,2}};
	
	private static IndexedFaceSetFactory getDefaultFundamentalRegionFactory(TriangleGroup tg)	{
		double[][] vertices = tg.getTriangle();
		if (vertices == null)	return null;
		IndexedFaceSetFactory ifsf = IndexedFaceSetUtility.constructPolygonFactory(null, vertices, Pn.EUCLIDEAN);
		ifsf.update();
		return ifsf;
	}


}
