/**
 * 
 */
package template.kaleidoscope;


import static de.jreality.shader.CommonAttributes.DIFFUSE_COLOR;
import static de.jreality.shader.CommonAttributes.LINE_SHADER;
import static de.jreality.shader.CommonAttributes.POLYGON_SHADER;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;

import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.SwingConstants;

import template.Assignment;
import de.jreality.geometry.IndexedFaceSetFactory;
import de.jreality.geometry.IndexedLineSetFactory;
import de.jreality.math.MatrixBuilder;
import de.jreality.math.Pn;
import de.jreality.math.Rn;
import de.jreality.plugin.JRViewer;
import de.jreality.plugin.content.ContentAppearance;
import de.jreality.scene.Appearance;
import de.jreality.scene.IndexedFaceSet;
import de.jreality.scene.SceneGraphComponent;
import de.jreality.scene.Viewer;
import de.jreality.scene.data.Attribute;
import de.jreality.scene.tool.ToolContext;
import de.jreality.shader.DefaultGeometryShader;
import de.jreality.shader.ImplodePolygonShader;
import de.jreality.shader.ShaderUtility;
import de.jreality.tools.DraggingTool;
import de.jreality.tools.RotateTool;
import de.jreality.util.CameraUtility;
import de.jreality.util.SceneGraphUtility;
import de.jtem.discretegroup.core.DirichletDomain;
import de.jtem.discretegroup.core.DiscreteGroup;
import de.jtem.discretegroup.core.DiscreteGroupElement;
import de.jtem.discretegroup.core.DiscreteGroupSceneGraphRepresentation;
import de.jtem.discretegroup.core.DiscreteGroupSimpleConstraint;
import de.jtem.discretegroup.util.TextSlider;
import de.jtem.jrworkspace.plugin.Controller;
import de.jtem.jrworkspace.plugin.simplecontroller.SimpleController.PropertiesMode;

/**
 *  This class models a 3D Kaleidoscope in the sides of a 3D tetrahedron, each side working as a mirror. 
 *  The object to be tessellated can either be chosen as the intersection of a 3D hyperplane and the tetrahedron, 
 *  where the hyperplane can be rotated and translated arbitrarily, or as a geometry loaded by the user. 
 *  Furthermore, among other features, one of the mirrors can be disabled, leaving the Kaleidoscope with a maximum of 
 *  48 different reflections.
 * @author nervi
 *
 */
public class Kaleidoscope extends Assignment {
	public final static double[][] cubePlanes = { { 0, 0, 1, -1 },
			{ 0, 0, 1, 1 }, { 0, 1, 0, -1 }, { 0, 1, 0, 1 }, { 1, 0, 0, -1 },
			{ 1, 0, 0, 1 } };
	public final static double[] xplane = { 0, 0, 1, -1 }, yplane = { -1, 0, 0,
			0 }, zplane = { 0, 1, -1, 0 }, wplane = { 1, -1, 0, 0 };
	public final static double[][] planes = { xplane, yplane, zplane, wplane };
	transient private SceneGraphComponent World,
		tetra = new SceneGraphComponent("tetra"),
		fundDomSGC = new SceneGraphComponent("fundDomSGC"),
		singleFaceSGC;
	transient private boolean showTetra = false;

	transient private DiscreteGroup dgCube;
	transient private DiscreteGroupSceneGraphRepresentation dgsgr, dgsgrCube;
	transient private TetrahedralKaleidoscope dg;
	transient private TetrahedralSlicer slicer = new TetrahedralSlicer();
	transient private int copies = 48, cubeCopies = 1;
	transient private double implodeFactor = 0.2;
	double[] cuttingPlane;

	/**
	 * Main method, only used to generate an object of this class
	 * @param args
	 */
	public static void main(String[] args)		{
		Kaleidoscope theProgram = new Kaleidoscope();
		theProgram.display();
	}
/**
 * Sets up this World
 */
	public SceneGraphComponent getContent() {
		// set up the discrete group which generates reflections in 3 of the 4 faces
		// of the tetrahedron
		dg = new TetrahedralKaleidoscope(planes, Pn.EUCLIDEAN);
		dg.setActivePlanes(new int[]{1,2,3});
		dg.update();
		// set the slicer to correspond to this tetrahedron
		slicer.setTetrahedraPlanes(planes);
		if (cuttingPlane != null) slicer.setPlaneEqn(cuttingPlane);
		slicer.update();
		
		// set up the discrete group for the "bigger" group which contains also the reflections
		// in the fourth face of the tetrahedron (whose fundamental region is the cube)
		getCubeGroup();

		// set the center point of the discrete group ... not sure why this needs to be done
		double[][] dirDomVerts = slicer.getTetrahedraVertices();
		double[] center = Rn.average(null, dirDomVerts);
		System.err.println("center = "+Rn.toString(center));
		dg.setCenterPoint(center);
		fundDomSGC.removeAllChildren();
		
		// This SGC will contain the geometry for our demo: a polygon generated
		// by clipping a plane to the tetrahedron.
		singleFaceSGC = SceneGraphUtility.createFullSceneGraphComponent("single face");
		fundDomSGC.addChild(singleFaceSGC);
		singleFaceSGC.setGeometry(slicer.getSlice()); 
		slicer.insertRotateSliceTool(singleFaceSGC);
		slicer.insertDragSliceTool(singleFaceSGC);
		
		// This class helps you build a jReality scene graph from a discrete group
		// build up two of these; the second one contains the first as its "fundamental domain"
		dg.setConstraint(new DiscreteGroupSimpleConstraint(cubeCopies > 1 ? 48 : copies));
		dg.update();			
		dgsgr = new DiscreteGroupSceneGraphRepresentation(dg);
		dgsgr.setWorldNode(fundDomSGC);
		dgsgr.update();
		dgCube.setConstraint(new DiscreteGroupSimpleConstraint(cubeCopies));
		dgsgrCube = new DiscreteGroupSceneGraphRepresentation(dgCube);
		dgsgrCube.setWorldNode(dgsgr.getRepresentationRoot());
		dgCube.update();
		dgsgrCube.update();

		// create the implode polygon shader
		Appearance appearance = fundDomSGC.getAppearance();
		DefaultGeometryShader dgs = (DefaultGeometryShader) 
   		ShaderUtility.createDefaultGeometryShader(appearance, true);
		ImplodePolygonShader ips = (ImplodePolygonShader) dgs.createPolygonShader("implode");
		appearance.setAttribute("polygonShader.implodeFactor", implodeFactor);
		
		// set up the single tetrahedron for optional display
		tetra.setGeometry(buildTetrah().getIndexedLineSet());
		Appearance apT = new Appearance();//tetra.getAppearance();
		apT.setAttribute(LINE_SHADER+"."+DIFFUSE_COLOR, new Color(20, 250, 250));
		apT.setAttribute(POLYGON_SHADER+"."+DIFFUSE_COLOR, new Color(250, 250, 20));
		tetra.setAppearance(apT);
		tetra.setVisible(showTetra);

		// put the result in the "world" sgc and return it
		World = SceneGraphUtility.createFullSceneGraphComponent("world");
		World.addChildren(dgsgrCube.getRepresentationRoot(), tetra);
		World.getAppearance().setAttribute("backgroundColor", Color.DARK_GRAY);

		return World;

	}
	
	// set up the group of reflections in the faces of one cube 
	// (which itself contains 48 of the tetrahedra)
	private void getCubeGroup() {
		dgCube = new DiscreteGroup();
		dgCube.setMetric(Pn.EUCLIDEAN);	
		dgCube.setDimension(3);			
		dgCube.setFinite(false);
		dgCube.setConstraint(new DiscreteGroupSimpleConstraint(cubeCopies));
		DiscreteGroupElement[] gensCube = new DiscreteGroupElement[6];
		for (int i = 0; i < 6; i++) 		
			 gensCube[i] = new DiscreteGroupElement( Pn.EUCLIDEAN, MatrixBuilder.euclidean().reflect(cubePlanes[i]).getArray(), "x" + i); //new DiscreteGroupElement(); // 
		dgCube.setGenerators(gensCube);	
		dgCube.update();
	}
		
		
	@Override
	public void setupJRViewer(JRViewer v) {
		super.setupJRViewer(v);
		v.registerPlugin(new ContentAppearance());
	
		de.jreality.plugin.basic.Scene scene = v.getPlugin(de.jreality.plugin.basic.Scene.class);
		MatrixBuilder.euclidean().assignTo(scene.getAvatarPath().getLastComponent());
		v.setPropertiesResource(Kaleidoscope.class, "kaleidoscope.xml");
	}
	
	@Override
	public void display() {
		super.display();

		Viewer viewer = jrviewer.getViewer();
		CameraUtility.encompass(viewer);
		viewer.getSceneRoot().getAppearance().setAttribute("backgroundColor", Color.DARK_GRAY);
		viewer.getSceneRoot().getAppearance().setAttribute("backgroundColors", Appearance.INHERITED);
		viewer.renderAsync();
	}

	@Override
	public Component getInspector() {
		Box vbox = Box.createVerticalBox();

		Box hbox = Box.createHorizontalBox();
		Box hbox2 = Box.createHorizontalBox();
		vbox.add(hbox);
		vbox.add(hbox2);
		
		 final TextSlider.Double implodeSlider = new TextSlider.Double("Implode", SwingConstants.HORIZONTAL, -1, 1, implodeFactor);
		 implodeSlider.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent arg0) {
					implodeFactor = ((TextSlider) arg0.getSource()).getValue().doubleValue();
										
					Appearance ap = World.getAppearance();
					
					ap.setAttribute("polygonShader.implodeFactor", implodeFactor);
					World.setAppearance(ap);

				}
		 });
		final TextSlider copiesSlider = new TextSlider.Integer("Copies",SwingConstants.HORIZONTAL, 1, 48, copies);
		copiesSlider.setToolTipText("# of copies in cube");
		
			copiesSlider.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				copies = ((TextSlider) arg0.getSource()).getValue().intValue();
				dg.setConstraint(new DiscreteGroupSimpleConstraint(copies));
				dg.update();
				dgsgr.setElementList(dg.getElementList());
				dgsgr.update();
				
			}
		});
		vbox.add(copiesSlider);
		final TextSlider cubeCopiesSlider = new TextSlider.Integer("Cube copies",SwingConstants.HORIZONTAL, 1, 25, cubeCopies);
		cubeCopiesSlider.setToolTipText("# of cubes");
		
			cubeCopiesSlider.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				cubeCopies = ((TextSlider) arg0.getSource()).getValue().intValue();
				dgCube.setConstraint(new DiscreteGroupSimpleConstraint(cubeCopies));
				dgCube.update();
				dgsgrCube.setElementList(dgCube.getElementList());
				dgsgrCube.update();
				
			}
		});
		vbox.add(cubeCopiesSlider);
		vbox.add(implodeSlider);
//		JCheckBox mittenDrinCB= new JCheckBox("Follow camera");
//		mittenDrinCB.setSelected(followCamera);
//		mittenDrinCB.addActionListener(new ActionListener() {
//			
//			@Override
//			public void actionPerformed(ActionEvent arg0) {
//				followCamera = ((JCheckBox)arg0.getSource()).isSelected();
//				dgsgr.setFollowsCamera(followCamera);
//				MatrixBuilder.euclidean().translate(0,0,followCamera ? 0 : 6).assignTo(dgsgr.getAvatarPath().getLastComponent());
//			}
//		});
		JCheckBox showTetrah= new JCheckBox("Show tetrahedron");
		showTetrah.setSelected(this.showTetra);
		showTetrah.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				tetra.setVisible((showTetra = ((JCheckBox)arg0.getSource()).isSelected()));
			}
		});
		
				
		vbox.add(showTetrah);
//		vbox.add(mittenDrinCB);
		packSecondViewer(vbox);
		return vbox;

	}
	
	// set up a second viewer and then pack it into this inspector as a sub-window
	// have to add some duplicate code, such as property file	
	private void packSecondViewer(Box vbox) {
		JRViewer jrv = new JRViewer();
		jrv.getController().setPropertiesMode(PropertiesMode.StaticPropertiesFile);
		jrv.setPropertiesInputStream(this.getClass().getResourceAsStream("kaleidoscope.xml"));
		
		SceneGraphComponent world = new SceneGraphComponent();
		final SceneGraphComponent cutSGC = new SceneGraphComponent();
	    final IndexedLineSetFactory tetraIlsf = buildTetrah();
	    cutSGC.setGeometry( slicer.getSlice());
	   
		world.addTool(new RotateTool());
		world.setGeometry(tetraIlsf.getIndexedLineSet());
		world.addChild(cutSGC);

		Appearance apT = new Appearance();
		apT.setAttribute(LINE_SHADER+"."+DIFFUSE_COLOR, new Color(20, 250, 250));
		apT.setAttribute(POLYGON_SHADER+"."+DIFFUSE_COLOR,new Color(220, 240, 250)); 
		apT.setAttribute("backgroundColor", Color.DARK_GRAY);
		world.setAppearance(apT);

		slicer.insertRotateSliceTool(cutSGC);
	
		jrv.setContent(world);
		jrv.startupLocal();
		Component component = (Component) jrv.getViewer().getViewingComponent();
		component.setMinimumSize(new Dimension(220, 300));
		vbox.add(component);
	}
	/*
	 * Displays the documentation of this program
	 */
	@Override
	public String getDocumentationFile() {
		return null;
	}
	
	/**
	 * 
	 * @return a tetrahdron in the shape of a IndexedLineSetFactory
	 */
	public static IndexedLineSetFactory buildTetrah()
	{
		IndexedLineSetFactory tetraIlsf = new IndexedLineSetFactory();
	    
	    double [][] vertices = new double[][] {
	      {0,0,0},  {0,1,1}, {0,0,1}, {1,1,1}
	    };
	    
	    int[][] edgeIndices = new int[][]{
	      {0, 1}, {0, 2}, {0, 3}, {1, 2} , {1,3}, {2,3}
	    };
	    tetraIlsf.setVertexCount( vertices.length );
	    tetraIlsf.setVertexCoordinates( vertices );
	    tetraIlsf.setLineCount(edgeIndices.length);
	    tetraIlsf.setEdgeIndices(edgeIndices);
	    tetraIlsf.update();
	    tetraIlsf.getGeometry().setName("tetrah");
	    return tetraIlsf;
	}
	
			
		public void restoreStates(Controller c) throws Exception {
			copies=c.getProperty(getClass(), "copies", copies);
			cubeCopies=c.getProperty(getClass(), "cubeCopies", cubeCopies);
			cuttingPlane=c.getProperty(getClass(), "cttgPlanes", cuttingPlane);
			implodeFactor=c.getProperty(getClass(), "implodeFactor", implodeFactor);
		}

		@Override
		public void storeStates(Controller c) throws Exception {
			System.err.println("Storing ");
			c.storeProperty(getClass(), "copies", copies);
			c.storeProperty(getClass(), "cubeCopies", cubeCopies);
			c.storeProperty(getClass(), "cttgPlanes", cuttingPlane);
			c.storeProperty(getClass(), "implodeFactor", implodeFactor);

		}
		@Override
		public File getPropertyFile() {
			super.getPropertyFile();
			// TODO Auto-generated method stub
			return null;
		}

}
	
	// commented-out features include animating the cutting plane and being able to fly in 
	// the middle of the tessellation rather than looking at it from "outside"
//			animationPlugin = jrviewer.getPlugin(AnimationPlugin.class);
//			for (int i = 0; i<4; ++i)	{
//				final int j = i;
//				KeyFrameAnimatedDelegate<Double> del = new KeyFrameAnimatedDelegate<Double>() {
	//
//					@Override
//					public void propagateCurrentValue(Double t) {
//						cuttingPlane[j] = t;
//						 if (j == 3) updateCutPolygon();
//					}
	//
//					@Override
//					public Double gatherCurrentValue(Double t) {
//						return cuttingPlane[j];
//					}
//				};
//				KeyFrameAnimatedDouble cutPlaneKF = new KeyFrameAnimatedDouble(del);
//				cutPlaneKF.setName("planeEquation"+i);
//				animationPlugin.getAnimated().add(cutPlaneKF);
//				
//			}
//			
//			// load animation
//			ImportExport.readInto(animationPlugin.getAnimationPanel(), 
//					this.getClass().getResourceAsStream("ProjOckReh-anim.xml"));
//			
//			// add the feature of being in the middle of the tessellation
////			dgsgr.setFollowsCamera(followCamera);
//			SceneGraphPath pathToDG = SceneGraphUtility.getPathsBetween(viewer.getSceneRoot(), World).get(0);
//			pathToDG.pop();
//			dgsgr.attachToViewer(viewer, pathToDG, followCamera, 500, false, 500);
//			dgsgr.setViewer(jrviewer.getViewer());
//			de.jreality.plugin.basic.Scene scene = jrviewer.getPlugin(de.jreality.plugin.basic.Scene.class);
//			SceneGraphPath pathToAvatar = scene.getAvatarPath();
//			dgsgr.setAvatarPath(pathToAvatar);
//			dgsgr.update();
//			MatrixBuilder.euclidean().translate(0,0,followCamera ? 0 : 6).assignTo(dgsgr.getAvatarPath().getLastComponent());
//			FlyTool flytool = new FlyTool();
//			flytool.setGain(.15);
//			pathToAvatar.getLastComponent().addTool(flytool);
//			CameraUtility.getCamera(viewer).setFieldOfView(75.0);
//			// move the lights to move with the camera
//			de.jreality.scene.Scene.executeWriter(viewer.getSceneRoot(), new Runnable() {
//				
//				@Override
//				public void run() {
//					final SceneGraphComponent lights = SceneGraphUtility.getPathsToNamedNodes(viewer.getSceneRoot(), "lights").get(0).getLastComponent();
//					viewer.getSceneRoot().removeChild(lights);
//					CameraUtility.getCameraNode(viewer).addChild(lights);
//					return;
//				}
//			});
			
//			viewer.getSceneRoot().getAppearance().setAttribute("lightingEnabled", false);
		

