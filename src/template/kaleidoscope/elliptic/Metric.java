package template.kaleidoscope.elliptic;

import java.awt.Color;

import charlesgunn.jreality.newtools.FlyTool;
import charlesgunn.jreality.tools.RotateShapeTool;
import charlesgunn.jreality.tools.TranslateShapeTool;
import de.jreality.math.MatrixBuilder;
import de.jreality.math.P3;
import de.jreality.math.Pn;
import de.jreality.plugin.basic.Scene;
import de.jreality.plugin.basic.View;
import de.jreality.scene.Appearance;
import de.jreality.scene.PointLight;
import de.jreality.scene.SceneGraphComponent;
import de.jreality.scene.Viewer;
import de.jreality.shader.CommonAttributes;
import de.jreality.util.CameraUtility;
import de.jreality.util.SceneGraphUtility;
import de.jtem.jrworkspace.plugin.Controller;
import de.jtem.jrworkspace.plugin.Plugin;

public class Metric extends Plugin {

//	View view;
//	Scene scene;
	Controller c;
	int metric = Pn.ELLIPTIC;
	Viewer viewer;
	@Override
	public void install(Controller con) throws Exception {
		super.install(c);
//		view = c.getPlugin(View.class);
//		scene =c.getPlugin(Scene.class);
		System.err.println("installing metric plugin");
		this.c = con;
	}

	/** 
	 * Update the scene graph (generally as a result of changed metric
	 * This involves:
	 * 	resetting the near/far clipping planes
	 *  resetting the transformation of the world, the camera node, and the light node
	 *  turning on the OpenGL shader for the non-euclidean cases
	 *  making the hyperbolic boundary sphere visible if the metric is ... hyperbolic
	 */
	public void setupMetric(int m, Viewer v)	{
		metric = m;
		viewer = v;
		FlyTool flytool = new FlyTool();
		flytool.setGain(.1);
//		Viewer viewer = view.getViewer().getCurrentViewer();
		CameraUtility.getCameraNode(viewer).addTool(flytool);
		SceneGraphUtility.removeLights(viewer);
		Appearance ap = viewer.getSceneRoot().getAppearance();
		ap.setAttribute(CommonAttributes.METRIC, metric);
		// this is all we have to do to tell the backend to use the non-euclidean vertex shader
		ap.setAttribute("useGLSL",true);
		ap.setAttribute("oneGLSL",true);
//		ap.setAttribute("lineShader.useGLSL", false);
//		MatrixBuilder.euclidean().scale(2.0).assignTo(scene.getAvatarComponent());

		resetScene();
	}

	double[][] falloffs =   {{1.5,.25,0},{.5,.5,0},{.5, .5, 0}};
	double[][] cameraClips = {{.001,2},{.01, 1000},{.01,-.05}};
	double distance = .5;
	double[] unitD = {Math.tanh(distance), distance, Math.tan(distance)};
	/**
	 * resets the transformations of the scene camera, avatar, and light nodes.
	 */
	public void resetScene() {
		CameraUtility.getCamera(viewer).setNear(cameraClips[metric+1][0]);
		CameraUtility.getCamera(viewer).setFar( cameraClips[metric+1][1]);

		MatrixBuilder.init(null, metric).translate(0,0,unitD[metric+1]).assignTo(lightNode);
		MatrixBuilder.init(null, metric).translate(0,0,0).
		assignTo(CameraUtility.getCameraNode(viewer));
		Scene scene = c.getPlugin(Scene.class);
//		MatrixBuilder.euclidean().scale(2.0).assignTo(scene.getAvatarComponent());
	}

	SceneGraphComponent lightNode = new SceneGraphComponent();
	/**
	 * initializes program lights
	 * @return {@link SceneGraphComponent} with light information
	 */
	public SceneGraphComponent makeLights() {
		lightNode.setName("lights");
		double f = -1.2;
		double[][] lightlocations = {
				{1,1,1,f},
				{1,-1,-1,f},
				{-1,-1,1,f},
				{-1,1,-1,f}
		};
		for( int i = 0; i < 4; ++i )	{
//			double[] axis = {-1.2,-1.6,-2,1};
//			if( i > 0 )
//				axis[i-1] = 1; 
//			else 
//				axis = new double[]{1.8,1.3,1,1};
			SceneGraphComponent l0 = SceneGraphUtility.createFullSceneGraphComponent("light0");
			PointLight pointLight = new PointLight();
			int c[] = {255, 255, 255};
			if( i < 3 )
				c[i] = 200;
			pointLight.setColor(new Color(c[0], c[1], c[2]));
			pointLight.setIntensity(.35);
			l0.getTransformation().setMatrix( P3.makeTranslationMatrix(null, lightlocations[i], metric));		
			l0.setLight(pointLight);
			lightNode.addChild(l0);
			//			dl.setFalloff(falloffs[metric+1]);
		}
		return lightNode;
	}

}
