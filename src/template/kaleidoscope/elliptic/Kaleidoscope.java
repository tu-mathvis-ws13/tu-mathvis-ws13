/**
 * 
 */
package template.kaleidoscope.elliptic;


import static de.jreality.shader.CommonAttributes.DIFFUSE_COLOR;
import static de.jreality.shader.CommonAttributes.LINE_SHADER;
import static de.jreality.shader.CommonAttributes.POLYGON_SHADER;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.List;

import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.SwingConstants;

import template.Assignment;
import charlesgunn.jreality.newtools.FlyTool;
import charlesgunn.jreality.plugin.TermesSpherePlugin;
import charlesgunn.jreality.tools.RotateShapeTool;
import charlesgunn.jreality.tools.TranslateShapeTool;
import de.jreality.geometry.IndexedLineSetFactory;
import de.jreality.math.MatrixBuilder;
import de.jreality.math.P3;
import de.jreality.math.Pn;
import de.jreality.math.Rn;
import de.jreality.plugin.JRViewer;
import de.jreality.plugin.basic.Content;
import de.jreality.plugin.basic.Scene;
import de.jreality.plugin.content.ContentAppearance;
import de.jreality.plugin.content.ContentTools;
import de.jreality.scene.Appearance;
import de.jreality.scene.Camera;
import de.jreality.scene.SceneGraphComponent;
import de.jreality.scene.Viewer;
import de.jreality.scene.SceneGraphPath;
import de.jreality.shader.CommonAttributes;
import de.jreality.shader.DefaultGeometryShader;
import de.jreality.shader.DefaultLineShader;
import de.jreality.shader.ImplodePolygonShader;
import de.jreality.shader.ShaderUtility;
import de.jreality.util.CameraUtility;
import de.jreality.util.SceneGraphUtility;
import de.jtem.discretegroup.core.DiscreteGroup;
import de.jtem.discretegroup.core.DiscreteGroupSceneGraphRepresentation;
import de.jtem.discretegroup.core.DiscreteGroupSimpleConstraint;
import de.jtem.discretegroup.groups.Spherical3DGroup;
import de.jtem.discretegroup.groups.TriangleGroup;
import de.jtem.discretegroup.util.TextSlider;
import de.jtem.jrworkspace.plugin.Controller;
import de.jtem.jrworkspace.plugin.Plugin;
import de.jtem.jrworkspace.plugin.simplecontroller.SimpleController.PropertiesMode;
import de.jtem.projgeom.PlueckerLineGeometry;

/**
 *  This class models a 3D Kaleidoscope in the sides of a 3D tetrahedron, each side working as a mirror. 
 *  The object to be tessellated can either be chosen as the intersection of a 3D hyperplane and the tetrahedron, 
 *  where the hyperplane can be rotated and translated arbitrarily, or as a geometry loaded by the user. 
 *  Furthermore, among other features, one of the mirrors can be disabled, leaving the Kaleidoscope with a maximum of 
 *  48 different reflections.
 * @author nervi
 *
 */
public class Kaleidoscope extends Assignment {
	public final  double[][] cubePlanes = { { 0, 0, 1, -1 },
			{ 0, 0, 1, 1 }, { 0, 1, 0, -1 }, { 0, 1, 0, 1 }, { 1, 0, 0, -1 },
			{ 1, 0, 0, 1 } };
	public final  double[] xplane = { 0, 0, 1, -1 }, yplane = { -1, 0, 0,
			0 }, zplane = { 0, 1, -1, 0 }, wplane = { 1, -1, 0, 0 };
	public final  double[][] planes = { xplane, yplane, zplane, wplane };
	transient private SceneGraphComponent World,
		tetra = new SceneGraphComponent("tetra"),
		fundDomSGC = new SceneGraphComponent("fundDomSGC"),
		singleFaceSGC;
	transient private boolean showTetra = false;

	transient private DiscreteGroup dgCube;
	transient private DiscreteGroupSceneGraphRepresentation dgsgr, dgsgrCube;
	transient private TetrahedralKaleidoscope dg;
	transient private TetrahedralSlicer slicer = new TetrahedralSlicer();
	transient private int copies = 120, cubeCopies = 1;
	transient private double implodeFactor = 0.2;
	double[] cuttingPlane;
	int metric = Pn.ELLIPTIC; //Pn.EUCLIDEAN; // 
	Metric metricPlugin = new Metric();

	/**
	 * given pt1 and pt2 determining a line, find the plane through pt1 perpendicular to the line pt1-pt2
	 * @param dst
	 * @param pt1
	 * @param pt2
	 * @param metric
	 * @return
	 */
	public static double[] getPlanePerpToLineThruPoint(double[] dst, double[] pt1, double[] pt2, int metric ) {
		// find the polar (orthogonal) plane to pt1
		double[] polarPl1 = Pn.polarizePoint(null, pt1, metric);
		// find its intersection with the line determined by pt1 and pt2
		double[] orthoPt1 = P3.lineIntersectPlane(dst, pt1, pt2, polarPl1);
		// find the polar plane of this point.  It will contain pt1 and will be perpendicular
		return Pn.polarizePoint(dst, orthoPt1, metric);
	}
	
	public static double[] getPlanePerpToLineThruPoint2(double[] dst, double[] pt1, double[] pt2, int metric ) {
		// find the line determined by pt1 and pt2
		double[] m = PlueckerLineGeometry.lineFromPoints(null, pt1, pt2);
		// find the polar (orthogonal) line to m
		double[] pPerp = PlueckerLineGeometry.polarize(null, m, Pn.ELLIPTIC);
		// find its intersection with the line determined by pt1 and pt2
		return PlueckerLineGeometry.lineJoinPoint(null, pPerp, pt1);
	}
	
	public static double[][] get235Planes()	{
		TriangleGroup dg = TriangleGroup.instanceOfGroup("*235");
		double[][] verts = dg.getTriangle();
		double[][] planes = new double[4][4];
		for (int i= 0; i<verts.length; ++i)	{
			planes[i] = P3.planeFromPoints(null, verts[i],P3.originP3,  verts[(i+1)%3]);
		}
//		System.err.println("triangle = "+Rn.toString(verts));
		double angle = Math.acos(1.0/(2 * verts[2][0]));
		double c = Math.cos(angle), s = Math.sin(angle);
		for (int i = 0; i<3; ++i)
			planes[3][i] = c * verts[2][i];
		planes[3][3] = -s;
		double[] cp = {-0.383587,	0.0288519,	-0.659993,	0.250000};
		for (int i = 0; i<4; ++i)	{
			Pn.normalize(planes[i], planes[i], Pn.ELLIPTIC);
			double d = Rn.innerProduct(planes[i], cp);
			System.err.println("d cp = "+d);
		}
		System.err.println("planes = "+Rn.toString(planes));
		double[] angles = new double[6];
		int count = 0;
		for (int i = 0; i<3; ++i)	{
			for (int j = i+1;j<4; ++j)	{
				angles[count++] = Pn.angleBetween(planes[i], planes[j], Pn.ELLIPTIC);
			}
		}
		System.err.println("angles = "+Rn.toString(Rn.times(null, 180.0/Math.PI, angles)));
		return planes;
	}

	public static void main(String[] args)		{
		Kaleidoscope theProgram = new Kaleidoscope();
		theProgram.display();
	}

	public SceneGraphComponent getContent() {
		// set the slicer to correspond to this tetrahedron
		slicer.setTetrahedraPlanes(dg.getPlanes());
//		if (cuttingPlane != null) slicer.setPlaneEqn(cuttingPlane);
		slicer.update();
		
		// set up the discrete group for the "bigger" group which contains also the reflections
		// in the fourth face of the tetrahedron (whose fundamental region is the cube)
		getCubeGroup();

		// set the center point of the discrete group ... not sure why this needs to be done
		double[][] dirDomVerts = slicer.getTetrahedraVertices();
		double[] center = Rn.average(null, dirDomVerts);
		System.err.println("center = "+Rn.toString(center));
//		dg.setCenterPoint(center);
		fundDomSGC.removeAllChildren();
		
		// This SGC will contain the geometry for our demo: a polygon generated
		// by clipping a plane to the tetrahedron.
		singleFaceSGC = SceneGraphUtility.createFullSceneGraphComponent("single face");
		singleFaceSGC.getAppearance().setAttribute(CommonAttributes.VERTEX_DRAW, false);
		singleFaceSGC.getAppearance().setAttribute("lineShader.tubeRadius", .004);
		fundDomSGC.addChild(singleFaceSGC);
		singleFaceSGC.setGeometry(slicer.getSlice()); //buildTetrah().getGeometry()); //
		slicer.insertRotateSliceTool(singleFaceSGC);
		slicer.insertDragSliceTool(singleFaceSGC);
		
		// This class helps you build a jReality scene graph from a discrete group
		// build up two of these; the second one contains the first as its "fundamental domain"
//		dg.setConstraint(new DiscreteGroupSimpleConstraint(cubeCopies > 1 ? 48 : copies));
		dg.update();			
		dgsgr = new DiscreteGroupSceneGraphRepresentation(dg);
		dgsgr.setWorldNode(fundDomSGC);
		dgsgr.update();
		dgCube.setConstraint(new DiscreteGroupSimpleConstraint(cubeCopies));
		dgsgrCube = new DiscreteGroupSceneGraphRepresentation(dgCube);
		dgsgrCube.setWorldNode(dgsgr.getRepresentationRoot());
		dgCube.update();
		dgsgrCube.update();

		// create the implode polygon shader
		Appearance appearance = fundDomSGC.getAppearance();
		DefaultGeometryShader dgs = (DefaultGeometryShader) 
   		ShaderUtility.createDefaultGeometryShader(appearance, true);
		ImplodePolygonShader ips = (ImplodePolygonShader) dgs.createPolygonShader("implode");
		DefaultLineShader dls = (DefaultLineShader) dgs.createLineShader("default");
		dls.createPolygonShader("default");
		appearance.setAttribute("polygonShader.implodeFactor", implodeFactor);
		
		// set up the single tetrahedron for optional display
		tetra.setGeometry(buildTetrah().getIndexedLineSet());
		Appearance apT = new Appearance();//tetra.getAppearance();
		apT.setAttribute(LINE_SHADER+"."+DIFFUSE_COLOR, new Color(20, 250, 250));
		apT.setAttribute(POLYGON_SHADER+"."+DIFFUSE_COLOR, new Color(250, 250, 20));
		tetra.setAppearance(apT);
		tetra.setVisible(showTetra);

		// put the result in the "world" sgc and return it
		World = SceneGraphUtility.createFullSceneGraphComponent("world");
		World.addChildren(dgsgrCube.getRepresentationRoot(), tetra); //dgsgrCube.getRepresentationRoot(), tetra);
		World.getAppearance().setAttribute("backgroundColor", Color.DARK_GRAY);
		World.addTool(new RotateShapeTool());
		World.addTool(new TranslateShapeTool());

		return World;
	}
	
	// set up the group of reflections in the faces of one cube 
	// (which itself contains 48 of the tetrahedra)
	private void getCubeGroup() {
		dgCube = Spherical3DGroup.instanceOf("533");	// 120-cell
		dgCube.setConstraint(new DiscreteGroupSimpleConstraint(cubeCopies));
//		dgCube = new DiscreteGroup();
//		dgCube.setMetric(Pn.EUCLIDEAN);	
//		dgCube.setDimension(3);			
//		dgCube.setFinite(false);
//		dgCube.setConstraint(new DiscreteGroupSimpleConstraint(cubeCopies));
//		DiscreteGroupElement[] gensCube = new DiscreteGroupElement[6];
//		for (int i = 0; i < 6; i++) 		
//			 gensCube[i] = new DiscreteGroupElement( Pn.EUCLIDEAN, MatrixBuilder.euclidean().reflect(cubePlanes[i]).getArray(), "x" + i); //new DiscreteGroupElement(); // 
//		dgCube.setGenerators(gensCube);	
//		dgCube.update();
	}
		
	@Override
	public List<Plugin> getPluginsToRegister() {
		// TODO Auto-generated method stub
		super.getPluginsToRegister();
		pluginsToLoad.add(metricPlugin);
		pluginsToLoad.add(new ContentAppearance());
		pluginsToLoad.add(new TermesSpherePlugin());
		Plugin toRemove = null;
		for (Plugin pl : pluginsToLoad) {
			if (pl instanceof ContentTools) {
				toRemove = pl;
				break;
			}
		}
		if (toRemove != null) pluginsToLoad.remove(toRemove);
		return pluginsToLoad;
	}

	@Override
	public void setupJRViewer(JRViewer v) {
		super.setupJRViewer(v);
		de.jreality.plugin.basic.Scene scene = v.getPlugin(de.jreality.plugin.basic.Scene.class);
		MatrixBuilder.euclidean().assignTo(scene.getAvatarPath().getLastComponent());
//		SceneGraphPath toWorld = (SceneGraphPath) scene.getContentPath().clone();
//		toWorld.push(World);
//		scene.setEmptyPickPath(toWorld);
//		v.setPropertiesResource(Kaleidoscope.class, "kaleidoscope.xml");
	}
	
	@Override
	public void display() {
		// set up the discrete group which generates reflections in 3 of the 4 faces
		// of the tetrahedron		
		dg = new TetrahedralKaleidoscope(get235Planes(), metric);
		dg.setConstraint(new DiscreteGroupSimpleConstraint(120));
		dg.setActivePlanes(new int[]{0,1,2});
		dg.update();

		super.display();
		metricPlugin.setupMetric(metric, jrviewer.getViewer());
		Viewer viewer = jrviewer.getViewer();
		CameraUtility.getCameraNode(viewer).addChild(metricPlugin.makeLights());
		Appearance ap = viewer.getSceneRoot().getAppearance();
		Camera cam = CameraUtility.getCamera(viewer);
		cam.setFieldOfView(60);
		MatrixBuilder.projective(metric).translate(0, 0, 1).assignTo(CameraUtility.getCameraNode(viewer));
		ap.setAttribute("backgroundColor", Color.DARK_GRAY);
		ap.setAttribute("backgroundColors", Appearance.INHERITED);
		viewer.renderAsync();
		Scene scene = jrviewer.getPlugin(Scene.class);
		SceneGraphPath sgp = SceneGraphUtility.getPathsBetween(viewer.getSceneRoot(), World).get(0);
		scene.setEmptyPickPath(sgp);
	}

	@Override
	public Component getInspector() {
		Box vbox = Box.createVerticalBox();

		Box hbox = Box.createHorizontalBox();
		Box hbox2 = Box.createHorizontalBox();
		vbox.add(hbox);
		vbox.add(hbox2);
		
		 final TextSlider.Double implodeSlider = new TextSlider.Double("Implode", SwingConstants.HORIZONTAL, -1, 1, implodeFactor);
		 implodeSlider.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent arg0) {
					implodeFactor = ((TextSlider) arg0.getSource()).getValue().doubleValue();				
					fundDomSGC.getAppearance().setAttribute("polygonShader.implodeFactor", implodeFactor);

				}
		 });
		final TextSlider copiesSlider = new TextSlider.Integer("Copies",SwingConstants.HORIZONTAL, 1, 48, copies);
		copiesSlider.setToolTipText("# of copies in cube");
		
			copiesSlider.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				copies = ((TextSlider) arg0.getSource()).getValue().intValue();
				dg.setConstraint(new DiscreteGroupSimpleConstraint(copies));
				dg.update();
				dgsgr.setElementList(dg.getElementList());
				dgsgr.update();
				
			}
		});
		vbox.add(copiesSlider);
		final TextSlider cubeCopiesSlider = new TextSlider.Integer("Dodec copies",SwingConstants.HORIZONTAL, 1, 25, cubeCopies);
		cubeCopiesSlider.setToolTipText("# of dodecs");
		
			cubeCopiesSlider.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				cubeCopies = ((TextSlider) arg0.getSource()).getValue().intValue();
				dgCube.setConstraint(new DiscreteGroupSimpleConstraint(cubeCopies));
				dgCube.update();
				dgsgrCube.setElementList(dgCube.getElementList());
				dgsgrCube.update();
				
			}
		});
		vbox.add(cubeCopiesSlider);
		vbox.add(implodeSlider);
//		JCheckBox mittenDrinCB= new JCheckBox("Follow camera");
//		mittenDrinCB.setSelected(followCamera);
//		mittenDrinCB.addActionListener(new ActionListener() {
//			
//			@Override
//			public void actionPerformed(ActionEvent arg0) {
//				followCamera = ((JCheckBox)arg0.getSource()).isSelected();
//				dgsgr.setFollowsCamera(followCamera);
//				MatrixBuilder.euclidean().translate(0,0,followCamera ? 0 : 6).assignTo(dgsgr.getAvatarPath().getLastComponent());
//			}
//		});
		JCheckBox showTetrah= new JCheckBox("Show tetrahedron");
		showTetrah.setSelected(this.showTetra);
		showTetrah.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				tetra.setVisible((showTetra = ((JCheckBox)arg0.getSource()).isSelected()));
			}
		});
		
				
		vbox.add(showTetrah);
//		vbox.add(mittenDrinCB);
		packSecondViewer(vbox);
		return vbox;

	}
	
	// set up a second viewer and then pack it into this inspector as a sub-window
	// have to add some duplicate code, such as property file	
	private void packSecondViewer(Box vbox) {
		JRViewer jrv = new JRViewer();
		jrv.getController().setPropertiesMode(PropertiesMode.StaticPropertiesFile);
		jrv.setPropertiesInputStream(this.getClass().getResourceAsStream("kaleidoscope.xml"));
		
		SceneGraphComponent world = new SceneGraphComponent();
		final SceneGraphComponent cutSGC = new SceneGraphComponent();
	    final IndexedLineSetFactory tetraIlsf = buildTetrah();
	    cutSGC.setGeometry( slicer.getSlice());
	   
		world.addTool(new RotateShapeTool());
		world.setGeometry(tetraIlsf.getIndexedLineSet());
		world.addChild(cutSGC);

		Appearance apT = new Appearance();
		apT.setAttribute(LINE_SHADER+"."+DIFFUSE_COLOR, new Color(20, 250, 250));
		apT.setAttribute(POLYGON_SHADER+"."+DIFFUSE_COLOR,new Color(220, 240, 250)); 
		apT.setAttribute("backgroundColor", Color.DARK_GRAY);
		world.setAppearance(apT);

		slicer.insertRotateSliceTool(cutSGC);
	
		jrv.setContent(world);
		jrv.startupLocal();
		Component component = (Component) jrv.getViewer().getViewingComponent();
		component.setMinimumSize(new Dimension(220, 300));
		vbox.add(component);
	}
	/*
	 * Displays the documentation of this program
	 */
	@Override
	public String getDocumentationFile() {
		return null;
	}
	
	/**
	 * 
	 * @return a tetrahdron in the shape of a IndexedLineSetFactory
	 */
	public  IndexedLineSetFactory buildTetrah()
	{
		IndexedLineSetFactory tetraIlsf = new IndexedLineSetFactory();
	    
	    double [][] vertices = new double[][] {
	      {0,0,0},  {0,1,1}, {0,0,1}, {1,1,1}
	    };
	    vertices = dg.getPoints();
	    int[][] edgeIndices = new int[][]{
	      {0, 1}, {0, 2}, {0, 3}, {1, 2} , {1,3}, {2,3}
	    };
	    tetraIlsf.setVertexCount( vertices.length );
	    tetraIlsf.setVertexCoordinates( vertices );
	    tetraIlsf.setEdgeCount(edgeIndices.length);
	    tetraIlsf.setEdgeIndices(edgeIndices);
	    tetraIlsf.update();
	    tetraIlsf.getGeometry().setName("tetrah");
	    return tetraIlsf;
	}
	
			
		public void restoreStates(Controller c) throws Exception {
			copies=c.getProperty(getClass(), "copies", copies);
			cubeCopies=c.getProperty(getClass(), "cubeCopies", cubeCopies);
			cuttingPlane=c.getProperty(getClass(), "cttgPlanes", cuttingPlane);
			implodeFactor=c.getProperty(getClass(), "implodeFactor", implodeFactor);
		}

		@Override
		public void storeStates(Controller c) throws Exception {
			System.err.println("Storing ");
			c.storeProperty(getClass(), "copies", copies);
			c.storeProperty(getClass(), "cubeCopies", cubeCopies);
			c.storeProperty(getClass(), "cttgPlanes", cuttingPlane);
			c.storeProperty(getClass(), "implodeFactor", implodeFactor);

		}
		@Override
		public File getPropertyFile() {
			super.getPropertyFile();
			// TODO Auto-generated method stub
			return null;
		}

}
	

	// commented-out features include animating the cutting plane and being able to fly in 
	// the middle of the tessellation rather than looking at it from "outside"
//			animationPlugin = jrviewer.getPlugin(AnimationPlugin.class);
//			for (int i = 0; i<4; ++i)	{
//				final int j = i;
//				KeyFrameAnimatedDelegate<Double> del = new KeyFrameAnimatedDelegate<Double>() {
	//
//					@Override
//					public void propagateCurrentValue(Double t) {
//						cuttingPlane[j] = t;
//						 if (j == 3) updateCutPolygon();
//					}
	//
//					@Override
//					public Double gatherCurrentValue(Double t) {
//						return cuttingPlane[j];
//					}
//				};
//				KeyFrameAnimatedDouble cutPlaneKF = new KeyFrameAnimatedDouble(del);
//				cutPlaneKF.setName("planeEquation"+i);
//				animationPlugin.getAnimated().add(cutPlaneKF);
//				
//			}
//			
//			// load animation
//			ImportExport.readInto(animationPlugin.getAnimationPanel(), 
//					this.getClass().getResourceAsStream("ProjOckReh-anim.xml"));
//			
//			// add the feature of being in the middle of the tessellation
////			dgsgr.setFollowsCamera(followCamera);
//			SceneGraphPath pathToDG = SceneGraphUtility.getPathsBetween(viewer.getSceneRoot(), World).get(0);
//			pathToDG.pop();
//			dgsgr.attachToViewer(viewer, pathToDG, followCamera, 500, false, 500);
//			dgsgr.setViewer(jrviewer.getViewer());
//			de.jreality.plugin.basic.Scene scene = jrviewer.getPlugin(de.jreality.plugin.basic.Scene.class);
//			SceneGraphPath pathToAvatar = scene.getAvatarPath();
//			dgsgr.setAvatarPath(pathToAvatar);
//			dgsgr.update();
//			MatrixBuilder.euclidean().translate(0,0,followCamera ? 0 : 6).assignTo(dgsgr.getAvatarPath().getLastComponent());
//			FlyTool flytool = new FlyTool();
//			flytool.setGain(.15);
//			pathToAvatar.getLastComponent().addTool(flytool);
//			CameraUtility.getCamera(viewer).setFieldOfView(75.0);
//			// move the lights to move with the camera
//			de.jreality.scene.Scene.executeWriter(viewer.getSceneRoot(), new Runnable() {
//				
//				@Override
//				public void run() {
//					final SceneGraphComponent lights = SceneGraphUtility.getPathsToNamedNodes(viewer.getSceneRoot(), "lights").get(0).getLastComponent();
//					viewer.getSceneRoot().removeChild(lights);
//					CameraUtility.getCameraNode(viewer).addChild(lights);
//					return;
//				}
//			});
			
//			viewer.getSceneRoot().getAppearance().setAttribute("lightingEnabled", false);
		

