package template.kaleidoscope.elliptic;

import de.jreality.geometry.IndexedFaceSetFactory;
import de.jreality.math.P3;
import de.jreality.math.Pn;
import de.jreality.math.Rn;
import de.jreality.scene.IndexedFaceSet;
import de.jreality.scene.SceneGraphComponent;
import de.jreality.scene.tool.ToolContext;
import de.jreality.tools.DraggingTool;
import de.jreality.tools.RotateTool;
/**
 * This class encapsulates a tetrahedron which knows how to slice a plane against its volume to
 *  produce a polygon.  It also provides rotate and drag tools for insertion into a scene graph which carry out
 *  this operation.
 * @author gunn
 *
 */
public class TetrahedralSlicer {

	// plane equation of slicing plane
	protected double[] planeEqn =  { 0,0,1,-.3 };
	// the tetrahedron
	protected double[][] tplanes, tverts = new double[4][];
	// the factory used to update the slice
	transient protected IndexedFaceSetFactory cutPolygonFactory = new IndexedFaceSetFactory();
	
	public TetrahedralSlicer()	{
		cutPolygonFactory.setVertexCount(6);
		cutPolygonFactory.setGenerateEdgesFromFaces( true );
		cutPolygonFactory.setGenerateFaceNormals( true );
		cutPolygonFactory.setGenerateVertexNormals( true );
		cutPolygonFactory.setFaceCount(1);
	    cutPolygonFactory.getGeometry().setName("cut polygon");
	}
	
	public IndexedFaceSet getSlice()	{
		return cutPolygonFactory.getIndexedFaceSet();
	}
	
	public double[] getPlaneEqn() {
		return planeEqn;
	}

	public void setPlaneEqn(double[] planeEqn) {
		this.planeEqn = planeEqn;
	}

	public double[][] getTetrahedraPlanes() {
		return tplanes;
	}

	public double[][] getTetrahedraVertices() {
		return tverts;
	}

	public void setTetrahedraPlanes(double[][] tetrahedraPlanes) {
		tplanes = tetrahedraPlanes;
		// calculate the vertices from the planes
		for (int i = 0; i<4; ++i)	{
			int i0 = (i+1)%4, i1 = (i+2)%4, i2 = (i+3)%4;
			tverts[i] = P3.pointFromPlanes(null, tplanes[i0], tplanes[i1], tplanes[i2]);
		}
		Pn.dehomogenize(tverts, tverts);
	}

	/**
	 * This modifies a standard rotate tool to create a new "rotated" version
	 * of the slice with each invocation.
	 * @param sgc
	 */
	public void insertRotateSliceTool(final SceneGraphComponent sgc)	{
		RotateTool rt =  new RotateTool() {
			@Override
			public void perform(ToolContext tc) {
				super.perform(tc);
				double[] cuttingPlane = getPlaneEqn();
				double[] m = sgc.getTransformation().getMatrix();
				
				double[] mj = Rn.transpose(null, Rn.inverse(null, m));
				cuttingPlane = Rn.matrixTimesVector(null, mj, cuttingPlane);
				comp.getTransformation().setMatrix(Rn.identityMatrix(4));					
				setPlaneEqn(cuttingPlane);
				update();
			}
		};
		sgc.addTool(rt);
	}

	/**
	 * This modifies a standard dragging tool to create a new "rotated" version
	 * of the slice with each invocation.
	 * @param sgc
	 */
	public void insertDragSliceTool(final SceneGraphComponent sgc)	{
		DraggingTool rt =  new DraggingTool() {
			@Override
			public void perform(ToolContext tc) {
				super.perform(tc);
				double[] cuttingPlane = getPlaneEqn();
				double[] m = sgc.getTransformation().getMatrix();
				
				double[] mj = Rn.transpose(null, Rn.inverse(null, m));
				cuttingPlane = Rn.matrixTimesVector(null, mj, cuttingPlane);
				comp.getTransformation().setMatrix(Rn.identityMatrix(4));					
				setPlaneEqn(cuttingPlane);
				update();
			}
		};
		sgc.addTool(rt);
	}
	
	// used for passing data between following two methods
	transient private int nCuts;
	transient protected boolean[] edgeCuts = new boolean[6];
	/**
	 * Computes the intersection points of the the plane and the
	 * tetrahedron
	 * @return array of intersection points
	 */
	private double[][] computeCuts2(){
		double[][] C= new double[6][4];
		nCuts = 0;
		edgeCuts = new boolean[6];
		System.err.println("plane = "+Rn.toString(planeEqn));
		for (int i = 0; i < 3; i++) { 			
			for (int j = i+1; j < 4; j++) {
				double pi1 = Rn.innerProduct(tverts[i], planeEqn);
				double pi2 = Rn.innerProduct(tverts[j], planeEqn);
				System.err.println("p1 = "+pi1+"pi2 = "+pi2);
				if (pi1 == 0.0 && pi2 == 0.0)	{// edge lies in the plane!

				}else if(pi1 == 0.0 || pi2 == 0.0){// one vertex lies in the plane!
					
				}else if(pi1*pi2<0) {
					nCuts += 1;
					double[] tmp = new double[4];
					double[] dst = new double[4];
					Rn.linearCombination(tmp, pi2, tverts[i], -pi1, tverts[j]);
					Pn.dehomogenize(dst, tmp);
					C[i+j-(i == 0 ? 1 : 0)] = dst;
					edgeCuts[i+j-(i == 0 ? 1 : 0)] = true;
//					System.out.println((dst[0] + " " +dst [1]+ " " + dst[2]);				
				}
				else{

				}
			}
		}
		System.out.println("found "+nCuts+" cuts.");
		return C;
	}
		
	/**
	 * Updates the visible intersection of the plane and the tetrahedron
	 */
	public void update(){
		double [][] vertices = computeCuts2();
        if (nCuts == 0) {
        	return;
        }
        System.err.println("cut verts = "+Rn.toString(vertices));
        cutPolygonFactory.setVertexCoordinates( vertices );
        // figure out the combinatorics of the resulting face:
        // in what order do the cut vertices appear in the polygon?
		  int [][] faceIndices = new int [1][nCuts];
	        if (nCuts == 3){
	        	int count=0;
	        	for (int i = 0; i < 6; i++) {
	        		if (edgeCuts[i]==true){
	        			faceIndices[0][count]= i;
	        			count += 1;
	        		}
	        	}
	        }
	        else if (nCuts == 4 ){
	      		if (edgeCuts[0]==false)
	      			faceIndices = new int[][]{{1,3,4,2}} ;
	      		else if (edgeCuts[1]==false)
	      			faceIndices = new int[][]{{0,2,5,3}} ;
	      		else if (edgeCuts[2]==false)
	      			faceIndices = new int[][]{{0,1,5,4}} ;
	      		else {
	      			throw new IllegalStateException("Problem with edge cut");
      		}
      	}

        cutPolygonFactory.setFaceIndices( faceIndices );	    	     
        cutPolygonFactory.update();    
	}
	
}
