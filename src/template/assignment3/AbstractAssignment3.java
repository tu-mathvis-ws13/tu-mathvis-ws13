package template.assignment3;

import java.awt.Component;
import java.io.File;
import java.io.IOException;

import charlesgunn.anim.gui.AnimationPanel;
import charlesgunn.jreality.texture.SimpleTextureFactory;
import charlesgunn.jreality.texture.SimpleTextureFactory.TextureType;

import de.jreality.geometry.ParametricSurfaceFactory;
import de.jreality.math.Matrix;
import de.jreality.math.MatrixBuilder;
import de.jreality.plugin.JRViewer;
import de.jreality.plugin.JRViewer.ContentType;
import de.jreality.scene.Appearance;
import de.jreality.scene.Camera;
import de.jreality.scene.SceneGraphComponent;
import de.jreality.shader.CommonAttributes;
import de.jreality.shader.DefaultGeometryShader;
import de.jreality.shader.DefaultPolygonShader;
import de.jreality.shader.ImageData;
import de.jreality.shader.PolygonShader;
import de.jreality.shader.ShaderUtility;
import de.jreality.shader.Texture2D;
import de.jreality.shader.TextureUtility;
import de.jreality.shader.TwoSidePolygonShader;
import de.jreality.util.CameraUtility;
import de.jreality.util.Color;
import de.jreality.util.Input;
import de.jreality.util.SceneGraphUtility;
import de.jtem.beans.InspectorPanel;
import template.Assignment;

abstract public class AbstractAssignment3 extends Assignment {

	SceneGraphComponent world = SceneGraphUtility.createFullSceneGraphComponent("world");
	private SimpleTextureFactory simpleTextureFactory = new SimpleTextureFactory();
	// allow toggle between VR and working textures
	boolean vrSupport = true;
	
	@Override
	public SceneGraphComponent getContent() {
		world.setGeometry(getFactory().getGeometry());
		Appearance ap = world.getAppearance();
		DefaultGeometryShader dgs = ShaderUtility.createDefaultGeometryShader(ap, true);
		dgs.setShowLines(false);
		dgs.setShowPoints(false);
		TwoSidePolygonShader tsps = (TwoSidePolygonShader) dgs.createPolygonShader("twoSide");
		DefaultPolygonShader dpsb =  (DefaultPolygonShader) tsps.createBack("default");
		DefaultPolygonShader dpsf =  (DefaultPolygonShader)tsps.createFront("default");
		dpsb.setDiffuseColor(java.awt.Color.white);
		dpsf.setDiffuseColor(java.awt.Color.yellow);
	
		// unfortunately the virtual reality plugin interferes with this texture
		// more precisely, the reflection maps used in the vr plugin combined with the twoSide shader
		// used here produces the interference.
		if (!vrSupport)	{
			// the following displays a texture on the surface but is incompatible with the two-sided shader
			simpleTextureFactory.setType(TextureType.WEAVE);
			simpleTextureFactory.setColor(1, java.awt.Color.green);
			simpleTextureFactory.setColor(2, new java.awt.Color(200, 125, 50));
			simpleTextureFactory.setAppearance(ap);
			simpleTextureFactory.update();
			ImageData id = simpleTextureFactory.getImageData();
			Texture2D tex2d = TextureUtility.createTexture(ap, "polygonShader", id);
			tex2d.setApplyMode(Texture2D.GL_MODULATE);
			Matrix tm = MatrixBuilder.euclidean().scale(8,8, 1).getMatrix();
			tex2d.setTextureMatrix(tm);
		}
		 //sigh this matrix op is to correct the way the avatar plugin is positioned
		if (vrSupport) 
			MatrixBuilder.euclidean().translate(0, 0, 16).assignTo(world);			

		return world;
	}

	@Override
	public void setupJRViewer(JRViewer v) {
		super.setupJRViewer(v);
		if (vrSupport) {
			v.addVRSupport();
		}
		v.addContentUI();
		v.addContentSupport(ContentType.Raw);
	}

	// this demonstrates how to use the class de.jtem.beans.InspectorPanel.InspectorPanel
	// to automatically generate inspection panels, in this case for the surface factory.
	@Override
	public Component getInspector()  {
		super.getInspector();
		InspectorPanel ip = new InspectorPanel();
		if (getFactory() == null) getContent();	// make sure the surface has been created
		ip.setObject(getFactory());
		ip.setUpdateMethod("update");		// don't actually need this, it's the default
		inspector.add(ip);
//		ip = new InspectorPanel();		// this isn't working yet: inspecting the texture factory
//		ip.setObject(simpleTextureFactory);
//		ip.setUpdateMethod("update");
//		inspector.add(ip);
		return inspector;
	}


	@Override
	public File getPropertyFile() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDocumentationFile() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void display() {
		// TODO Auto-generated method stub
		super.display();
		// VR plugin does silly things with the near/far clipping plane have to correct
		Camera cam = CameraUtility.getCamera(jrviewer.getViewer());
		cam.setNear(1.0);
		cam.setFar(200.0);
		// set up animation
//		try {
//			animationPlugin.getAnimationPanel().read(new Input(this.getClass().getResource(getAnimationFile())));
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		animationPlugin.getAnimationPanel();
		AnimationPanel.setResourceDir("src/");
	}
	
	abstract protected String getAnimationFile();
	abstract protected ParametricSurfaceFactory getFactory();
	

}
