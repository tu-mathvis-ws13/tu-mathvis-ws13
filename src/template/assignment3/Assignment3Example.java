package template.assignment3;

import java.io.File;

import de.jreality.geometry.ParametricSurfaceFactory;
import de.jreality.geometry.ParametricSurfaceFactory.Immersion;

/**
 * This assignment involves the following task:
 *     The evaluate() function below has been adjusted to roll up a square into a vertical cylinder.  For
 *     details see the source code of the method.
 *     Note: in contrast to the version shown in class  (Tuesday 12.11), the current version does not
 *     display a texture, but rather uses a "two-sided" shader to display one side as red and one side as yellow
 *     If you prefer to use textures you should turn off the virtual reality plugin
 *     (in the {@link AbstractAssignment3#setupJRViewer(de.jreality.plugin.JRViewer)()} method) and
 *     re-instate the texturing code below it.
 *     Note: also in contrast to the version show in class, this one incorporates a properties file and an animation 
 *     file, just like Assignment2. You can create your own or copy the ones from template/assignment3.
 * @author gunn
 *
 */
public class Assignment3Example extends AbstractAssignment3 {

	protected transient ParametricSurfaceFactory surface;
	double r = .5, R = 1.4, umin = -1, vmin = -1, umax = 1.0, vmax = 1.0;
	int uCount = 40, vCount = 40;
	double time = 0;
	@Override
	protected ParametricSurfaceFactory getFactory() {
		if (surface != null) return surface;
		
		Immersion im = new Immersion() {
			
			@Override
			public boolean isImmutable() {
				return false;
			}
			
			@Override
			public int getDimensionOfAmbientSpace() {
				return 4;
			}
			
			// The assignment is to do create an animation which begins with a square
			// and ends with the same square in the same position but facing the other direction.
			// The animation should not be possible to implement using matrices alone.
			@Override
			public void evaluate(double u, double v, double[] xyz, int index) {
				double angle = time * u * Math.PI;
				// The time t=0 is handled separately since it is not part of a normal cylinder
				// "time" has been set in the method setValueAtTime() (below).
				// Then r(t) = 1/(pi t) is the radius of the cylinder at  time t, and has
				// also been set in that method.
				// The conditions on the displayed section of the cylinder is that:
				//  1. It always has arclength 2. (This is determined by the condition that 
				//	     the initial square has sidelength 2.)
				//  2. The vertical mid-line of the square remains fixed (u = 0).
				// Condition 1) means the subtended angle is t*2*pi, (since the radius is 1/(t*pi))
				// and 2) means that the axis of the cylinder at time t has 
				// xyz coordinates (0, y, r(t)) for t != 0.
				xyz[0] = time == 0 ? u : r * Math.sin(angle);
				xyz[1] =  v;
				xyz[2] = time == 0 ? 0 : -r * Math.cos(angle) + r;
				xyz[3] = 1.0;
			}
		};
		surface = new ParametricSurfaceFactory(im);
		surface.setULineCount(uCount);
		surface.setVLineCount(vCount);
		surface.setUMin(umin);
		surface.setUMax(umax);
		surface.setVMin(vmin);
		surface.setVMax(vmax);
		surface.setGenerateFaceNormals(true);
		surface.setGenerateVertexNormals(true);
		surface.setGenerateEdgesFromFaces(true);
		surface.setClosedInUDirection(false);
		surface.setClosedInVDirection(false);
		surface.update();
		return surface;
	}

	@Override
	public void setValueAtTime(double d) {
		time = d;
		// set the radius for the cylinder, be careful at t=0
		r = (time == 0) ? -1 : 1/(Math.PI*time);
		surface.update();
	}

//	@Override
//	public File getPropertyFile() {
//		return new File("src/template/assignment3/assignment3.xml");
//	}
//
	@Override
	public String getDocumentationFile() {
		return "html/Assignment3.html";
	}


	@Override
	protected String getAnimationFile() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new Assignment3Example().display();

	}

}
